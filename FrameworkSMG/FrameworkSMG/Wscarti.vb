﻿Imports Newtonsoft.Json
Imports RestSharp

Module Wscarti

    Public Function inserisci_scarti(ordine_tesar As String, operation_no As String, macchina As String, codiceoperatoretesar As String, articolo As String, tiposcarto As String, qtascarto As Int32, codicescarto As String, sessiondev As Boolean) As Boolean
        Try
            Dim dirapi As String
            If sessiondev Then
                dirapi = "http://192.168.254.207:58081/api/it-IT/ProductionManagementPers/SetOrderProcessingScraps"
            Else
                dirapi = "http://192.168.254.112:58081/api/it-IT/ProductionManagementPers/SetOrderProcessingScraps"
            End If
            Dim client = New RestClient(dirapi)
            Dim request = New RestRequest(dirapi, Method.Post)
            request.AddHeader("Accept", "application/json")
            'request.AddJsonBody(SetScrap("2200051662", "020", "01-166", "0060", "01.32.150DH", "LA", 1, "0003"))
            request.AddJsonBody(SetScrap(ordine_tesar, operation_no, macchina, codiceoperatoretesar, articolo, tiposcarto, qtascarto, codicescarto))
            Dim response As RestResponse = client.Execute(request)
            If response.StatusCode = 200 Then

                Return True
            Else
                MsgBox(response.Content.ToString, MsgBoxStyle.Exclamation)
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function
    Public Function SetScrap(ProductionOrderId As String, OperationId As String, ResourceId As String, OperatorId As String, ItemId As String, Type As String, Quantity As Int32, ReasonCode As String)

        Dim sc As New ScrapData
        Dim itm As New ItemList
        sc.ProductionOrderId = ProductionOrderId
        sc.OperationId = OperationId
        sc.ResourceId = ResourceId
        sc.OperatorId = OperatorId
        itm.ItemId = ItemId
        itm.Type = Type
        itm.Quantity = Quantity
        itm.ReasonCode = ReasonCode
        sc.ScrapList.Add(itm)
        Return JsonConvert.SerializeObject(sc)


    End Function

    Public Class ScrapData
        Public Property ProductionOrderId As String
        Public Property OperationId As String
        Public Property ResourceId As String
        Public Property OperatorId As String

        Public Property ScrapList As New List(Of ItemList)
    End Class

    Public Class ItemList
        Public Property ItemId As String
        Public Property Type As String
        Public Property Quantity As Int32
        Public Property ReasonCode As String
    End Class
End Module
