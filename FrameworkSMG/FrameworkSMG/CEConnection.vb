﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports FileNet.Api.Core
Imports FileNet.Api.Collection
Imports System.Collections
Imports FileNet.Api.Util
Imports FileNet.Api.Authentication


Public Class CEConnection
        Private domain As IDomain
        Private ost As IObjectStoreSet
        Private osNames As ArrayList
        Private domainName As String
    Private isCredentialsEstablished1 As Boolean

    Public Sub New()
            domain = Nothing
            ost = Nothing
            osNames = New ArrayList()
            domainName = Nothing
        isCredentialsEstablished1 = False
    End Sub

    Public Sub EstablishCredentials(ByVal userName As String, ByVal password As String, ByVal uri As String)
            Dim cred As UsernameCredentials = New UsernameCredentials(userName, password)
            ClientContext.SetProcessCredentials(cred)
            Dim connection As IConnection = Factory.Connection.GetConnection(uri)
        isCredentialsEstablished1 = True
        IntializeVariables(connection)
        End Sub

        Private Sub IntializeVariables(ByVal connection As IConnection)
            domain = Factory.Domain.FetchInstance(connection, Nothing, Nothing)
            domainName = domain.Name
            ost = domain.ObjectStores
            SetOSNames()
        End Sub

        Private Sub SetOSNames()
            Dim ie As IEnumerator = ost.GetEnumerator()

            While ie.MoveNext()
                Dim os As IObjectStore = CType(ie.Current, IObjectStore)
                osNames.Add(os.DisplayName)
            End While
        End Sub

        Public Function GetOSNames() As ArrayList
            Return osNames
        End Function

        Public Function GetDomain() As IDomain
            Return domain
        End Function

        Public Function GetDomainName() As String
            Return domainName
        End Function

        Public Function IsCredentialsEstablished() As Boolean
            Return IsCredentialsEstablished
        End Function

        Public Function FetchOS(ByVal name As String) As IObjectStore
            Dim os As IObjectStore = Factory.ObjectStore.FetchInstance(domain, name, Nothing)
            Return os
        End Function
    End Class
