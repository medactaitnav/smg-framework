﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports FileNet.Api.Core
Imports FileNet.Api.Constants
Imports FileNet.Api.Collection
Imports FileNet.Api.Query
Imports System.Web
Imports System.Windows.Forms
Imports System.Net
Imports System.Threading
Imports SHDocVw
Imports System
Imports System.Globalization
Imports System.Drawing
Imports Microsoft.Reporting.WinForms
Imports System.Drawing.Printing
Imports System.Drawing.Imaging
Imports FileNet.Api.Security
Imports FileNet.Api.Property
Imports iTextSharp.text.pdf.parser
Imports iTextSharp.text.pdf
Imports System.Text.RegularExpressions
Imports RestSharp
Imports Newtonsoft.Json

Public Class FrameworkSMG
    Public Shared DatabaseNav As String
    Public Shared DatabaseSmg As String
    Public Shared connectionString As String
    Public Shared Autenticato As Boolean = False
    Public Shared sessiondev As Boolean = False
    Public Shared dirbatchrecord As String
    Public Shared loginuser As String
    Public Shared loginUserID As Integer
    Public Shared bUseReadUncommitted As Boolean = False
    Public Shared DataAdapterDictionary As New Dictionary(Of String, SqlClient.SqlDataAdapter)
    Public Shared DirScansioniBatchRecord As String = "\\SRV-ARCHIVE\Scansioni\batch record\"
    Public Shared path_scansioni_tray As String
    Public Shared path_scansioni_MAT As String
    Public Shared path_scansioni_PR As String
    Public Shared path_scansioni_lottoce As String
    Public Shared SetupDb As String = ""
    Public Shared idconn As Int32 = 1




    ''' <summary>
    ''' Return an encrypted string of the value passed.
    ''' </summary>
    Public Shared Function Encrypt(ByVal strText As String) As String
        Dim Hash As New System.Security.Cryptography.SHA1Managed
        Dim EncText As New System.Text.UnicodeEncoding
        Dim HashResult As Byte() = Hash.ComputeHash(EncText.GetBytes(strText))
        Try

            Hash = New System.Security.Cryptography.SHA1Managed
            EncText = New System.Text.UnicodeEncoding
            HashResult = Hash.ComputeHash(EncText.GetBytes(strText))
            Return CType(System.Convert.ToBase64String(HashResult), String)
        Catch ex As Exception

            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
            Return ""
        End Try
    End Function
    Public Shared Function request_token() As String
        Dim diToken As String
        diToken = " https://abacus.medacta.ch/oauth/oauth2/v1/token"

        Dim client = New RestClient(diToken)

        Dim request = New RestRequest(diToken, Method.Post)

        request.AddHeader("cache-control", "no-cache")
        request.AddHeader("content-type", "application/x-www-form-urlencoded")
        request.AddParameter("application/x-www-form-urlencoded", "grant_type=client_credentials&client_id=3d62e3d5-a2d1-ee01-186a-005056191388&client_secret=62ec0d77-07d1-ac9f-2373-87f7df7899d6", ParameterType.RequestBody)
        'request.AddJsonBody("'outputType': 'txt'")
        Dim response As RestResponse = client.Execute(request)
        request.AddHeader("authorization", "Bearer <access_token>")
        Dim VarToken As Token = JsonConvert.DeserializeObject(Of Token)(response.Content)
        Return VarToken.access_token
    End Function
    Public Class Token
        Public Property access_token As String

    End Class
    Public Class DatiPresenze
        Public Property id As String
        Public Property state As String
    End Class

    Public Class Parametri
        Public Property outputType As String
        Public Property parameters As ParametriDipendenti


    End Class
    Public Class ParametriDipendenti
        Public Property dipendente As String
        Public Property badge As String
        Public Property T3 As Boolean


    End Class

    Public Shared Function timbrature_abacus(dipendente As String, badge As String, t3 As Boolean) As String

        Try



            Dim dirpost As String
            dirpost = "https://abacus.medacta.ch/api/abareport/v1/report/100/presenze.avx"
            Dim client = New RestClient(dirpost)
            Dim requestpost = New RestRequest(dirpost, Method.Post)
            requestpost.AddHeader("authorization", "Bearer " & request_token())
            requestpost.AddHeader("content-type", "application/json")



            Dim parametridipendente As New ParametriDipendenti() With {
            .badge = badge, .dipendente = dipendente, .T3 = t3}

            Dim jsonParameters As String = JsonConvert.SerializeObject(parametridipendente, Formatting.Indented)


            Dim parametri As New Parametri() With {
            .outputType = "txt", .parameters = parametridipendente
             }

            Dim json As String = JsonConvert.SerializeObject(parametri, Formatting.Indented)
            requestpost.AddBody(json)
            Dim responsepost As RestResponse = client.Execute(requestpost)
            Dim VarId As DatiPresenze = JsonConvert.DeserializeObject(Of DatiPresenze)(responsepost.Content)
            If responsepost.StatusCode = 200 Then
                For i = 0 To 2
                    Dim dirget As String
                    dirget = "https://abacus.medacta.ch/api/abareport/v1/jobs/" & VarId.id
                    Dim requestget = New RestRequest(dirget, Method.Get)
                    requestget.AddHeader("authorization", "Bearer " & request_token())

                    Dim responseget As RestResponse = client.Execute(requestget)
                    Dim VarSatus As DatiPresenze = JsonConvert.DeserializeObject(Of DatiPresenze)(responseget.Content)
                    If VarSatus.state = "FinishedSuccess" Then
                        Dim dirgetReport As String
                        dirgetReport = "https://abacus.medacta.ch/api/abareport/v1/jobs/" & VarId.id & "/output"
                        Dim requestgetReport = New RestRequest(dirgetReport, Method.Get)
                        requestgetReport.AddHeader("authorization", "Bearer " & request_token())

                        Dim responsegetReport As RestResponse = client.Execute(requestgetReport)
                        Return responsegetReport.Content

                    End If

                    Threading.Thread.Sleep(500)
                Next



            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function


    Public Shared Function ExtractTextFromPdfFile(pdfFilePath As String) As String
        Dim extractedText As New StringBuilder()
        Try
            Dim reader As New PdfReader(pdfFilePath)
            For page As Integer = 1 To reader.NumberOfPages
                Dim its As New iTextSharp.text.pdf.parser.SimpleTextExtractionStrategy
                Dim s As String = PdfTextExtractor.GetTextFromPage(reader, page, its)
                s = Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(s)))
                extractedText.Append(s)
            Next
            reader.Close()
            reader.Dispose()
        Catch ex As Exception
            ' Gestisci eventuali eccezioni
        End Try

        Return extractedText.ToString()
    End Function


    Public Shared Function ExtractTextFromPdfstrema(pdfStream As Stream) As String
        Dim extractedText As New StringBuilder()
        Try
            Dim reader As New PdfReader(pdfStream)
            For page As Integer = 1 To reader.NumberOfPages
                Dim its As New iTextSharp.text.pdf.parser.SimpleTextExtractionStrategy
                Dim s As String = PdfTextExtractor.GetTextFromPage(reader, page, its)
                s = Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(s)))
                extractedText.Append(s)
            Next
            reader.Close()
            reader.Dispose()
        Catch ex As Exception
            ' Gestisci eventuali eccezioni
        End Try

        Return extractedText.ToString()
    End Function
    Public Shared Function ControllaLottoPDFTProduzione(file As String, lotto As String, tipo As Int32, usabalance As Int32) As Int32
        Try

            Dim str As String = ExtractTextFromPdfFile(file)
            If str.Contains(lotto) Then
                Dim dirfile = Split(file, "\")
                Dim ckfile = dirfile(dirfile.Length - 1).Replace(".pdf", "")
                Dim spdir = Split(ckfile, "_")
                Dim result As String = RemoveAlphanumeric(spdir(1))
                Dim sp = Split(str, lotto)
                sp = Split(UCase(sp(1)), "PRODUZIONE")
                Dim pos = sp(0).ToString.IndexOf(":")
                Dim pz_in As String = Mid(sp(0), pos + 7)
                sp = Split(pz_in, " ")
                Dim numero As Int32
                Try
                    Dim result2 As String = RemoveAlphanumeric(sp(1))

                    numero = CInt(result2)
                Catch ex As Exception
                    numero = 0
                End Try

                Dim numerofile = CInt(result)
                If numero <> numerofile Then
                    Return 1
                Else
                    Return 0
                End If

            Else
                Return 2
            End If
        Catch ex As Exception
            Return 3
        End Try
        ' Crea un oggetto PdfViewer e carica il documento PDF


    End Function
    Public Shared Function RemoveAlphanumeric(input As String) As String
        ' Utilizza un'espressione regolare per rimuovere tutti i caratteri alfanumerici
        Dim regex As New Regex("[^\d]")
        Return regex.Replace(input, String.Empty)
    End Function

    Public Shared Function ControllaLottoPDFT(idibm As String, lotto As String, usabalance As Int32)
        Dim documentoStream As Stream = download_ibmStream(idibm, 0, usabalance)
        ' Verifica se lo stream è valido e non vuoto
        If documentoStream IsNot Nothing AndAlso documentoStream.Length > 0 Then
            ' Crea un oggetto PdfViewer e carica il documento PDF
            Dim str As String = ExtractTextFromPdfstrema(documentoStream)

            If str.Contains(lotto) Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Public Shared Function getdocumentfromsupplierfolder(os As IObjectStore, lotto As String, PurchaseOrder As String, ItemNo As String, VMIOrder As String, OrdineCamp As String) As String
        'Dim aa = os.Id.ToString
        Dim sq As SearchSQL = New SearchSQL()
        Dim ss As SearchScope = New SearchScope(os)
        sq.SetMaxRecords(1)

        Dim rrsr As IRepositoryRowSet

        sq.SetQueryString("SELECT  id FROM DocumentFromSupplierFolderTemplate WHERE '" & lotto & "' IN [LotNo] and '" & PurchaseOrder & "' IN [PurchaseOrder] and '" & ItemNo & "' IN [ItemNo]")

        rrsr = ss.FetchRows(sq, Nothing, Nothing, Nothing)
        If rrsr.IsEmpty Then
            sq.SetQueryString("SELECT  id FROM DocumentFromSupplierFolderTemplate WHERE '" & PurchaseOrder & "' IN [PurchaseOrder] and '" & ItemNo & "' IN [ItemNo]")
            rrsr = ss.FetchRows(sq, Nothing, Nothing, Nothing)
        End If

        If VMIOrder <> "-" Then

            If rrsr.IsEmpty Then

                sq.SetQueryString("SELECT  id FROM DocumentFromSupplierFolderTemplate WHERE '" & lotto & "' IN [LotNo] and '" & VMIOrder & "' IN [PurchaseOrder] and '" & ItemNo & "' IN [ItemNo]")

                rrsr = ss.FetchRows(sq, Nothing, Nothing, Nothing)
            End If


            If rrsr.IsEmpty Then

                sq.SetQueryString("SELECT  id FROM DocumentFromSupplierFolderTemplate WHERE '" & VMIOrder & "' IN [PurchaseOrder] and '" & ItemNo & "' IN [ItemNo]")
                rrsr = ss.FetchRows(sq, Nothing, Nothing, Nothing)
            End If


        End If

        If OrdineCamp <> "-" Then
            If rrsr.IsEmpty Then
                sq.SetQueryString("SELECT  id FROM DocumentFromSupplierFolderTemplate WHERE '" & lotto & "' IN [LotNo] and '" & OrdineCamp & "' IN [PurchaseOrder] and '" & ItemNo & "' IN [ItemNo]")
                rrsr = ss.FetchRows(sq, Nothing, Nothing, Nothing)
            End If

            If rrsr.IsEmpty Then
                sq.SetQueryString("SELECT  id FROM DocumentFromSupplierFolderTemplate WHERE '" & OrdineCamp & "' IN [PurchaseOrder] and '" & ItemNo & "' IN [ItemNo]")
                rrsr = ss.FetchRows(sq, Nothing, Nothing, Nothing)
            End If
        End If

        If PurchaseOrder = "Storico" Then
            If rrsr.IsEmpty Then
                sq.SetQueryString("SELECT  id FROM DocumentFromSupplierFolderTemplate WHERE '" & lotto & "' IN [LotNo]")
            End If
            rrsr = ss.FetchRows(sq, Nothing, Nothing, Nothing)
        End If
        If rrsr.IsEmpty Then
            Return ""
        End If




        For Each n As IRepositoryRow In rrsr
            Return n.Properties.GetIdValue("id").ToString
        Next

    End Function




    Public Shared Function controlla_parametri(parametro As String, valore As String, macchina As String, stampo As String)
        Dim Adapter_Master As New SqlClient.SqlDataAdapter
        Dim connection As New SqlClient.SqlConnection(connectionString)
        connection.Open()

        Dim sql_read = "SELECT        dbo._PRODUZIONE_descrizione_macchine_stampi.mdc_machine, dbo._PRODUZIONE_descrizione_macchine_stampi_parametri.Stampo,  " &
                        "                         dbo._PRODUZIONE_descrizione_macchine_stampi_parametri." & parametro & "  " &
                        "FROM            dbo._PRODUZIONE_descrizione_macchine_stampi_parametri WITH(NOLOCK) INNER JOIN  " &
                        "                         dbo._PRODUZIONE_descrizione_macchine_stampi WITH(NOLOCK) ON   " &
                        "                         dbo._PRODUZIONE_descrizione_macchine_stampi_parametri.Stampo = dbo._PRODUZIONE_descrizione_macchine_stampi.Stampo  " &
                        "WHERE        (dbo._PRODUZIONE_descrizione_macchine_stampi.mdc_machine = '" & macchina & "') AND (dbo._PRODUZIONE_descrizione_macchine_stampi_parametri.Stampo = '" & stampo & "') "
        Dim command_Master_read = New SqlClient.SqlCommand(sql_read, connection)
        Dim r_read As SqlClient.SqlDataReader = command_Master_read.ExecuteReader()
        While (r_read.Read())
            Dim sp = Split(r_read(2), "±")
            Dim valmax As Decimal
            Dim valmin As Decimal
            Dim decimalSeparator As String = Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator
            If sp.Length > 1 Then
                Try
                    If decimalSeparator = "." Then
                        valore = Replace(valore, ",", ".")
                        valmax = CDec(Replace(sp(0), ",", ".")) + CDec(Replace(sp(1), ",", "."))
                        valmin = CDec(Replace(sp(0), ",", ".")) - CDec(Replace(sp(1), ",", "."))
                    Else
                        valore = Replace(valore, ".", ",")
                        valmax = CDec(Replace(sp(0), ".", ",")) + CDec(Replace(sp(1), ".", ","))
                        valmin = CDec(Replace(sp(0), ".", ",")) - CDec(Replace(sp(1), ".", ","))
                    End If

                    r_read.Close()
                    connection.Close()
                    If CDec(valore) >= valmin And CDec(valore) <= valmax Then
                        Return True
                    Else
                        MsgBox("Valore per il parametro (" & parametro & ") non compreso nel campo di tolleranza min: " & valmin & " max: " & valmax & " valore: " & valore, MsgBoxStyle.Exclamation)
                        Return False
                    End If
                Catch ex As Exception
                    MsgBox("Valore per il parametro (" & parametro & ") non compreso nel campo di tolleranza", MsgBoxStyle.Exclamation)
                    r_read.Close()
                    connection.Close()
                    Return False
                End Try

            Else
                sp = Split(r_read(2), "-")
                If sp.Length > 1 Then
                    Try
                        If decimalSeparator = "." Then
                            valore = Replace(valore, ",", ".")
                            valmax = CDec(Replace(sp(0), ",", ".")) + CDec(Replace(sp(1), ",", "."))
                            valmin = CDec(Replace(sp(0), ",", ".")) - CDec(Replace(sp(1), ",", "."))
                        Else
                            valore = Replace(valore, ".", ",")
                            valmax = CDec(Replace(sp(0), ".", ",")) + CDec(Replace(sp(1), ".", ","))
                            valmin = CDec(Replace(sp(0), ".", ",")) - CDec(Replace(sp(1), ".", ","))
                        End If

                        r_read.Close()
                        connection.Close()
                        If CDec(valore) >= valmin And CDec(valore) <= valmax Then
                            Return True
                        Else
                            MsgBox("Valore per il parametro (" & parametro & ") non compreso nel campo di tolleranza min: " & valmin & " max: " & valmax & " valore: " & valore, MsgBoxStyle.Exclamation)
                            Return False
                        End If
                    Catch ex As Exception
                        MsgBox("Valore per il parametro (" & parametro & ") non compreso nel campo di tolleranza", MsgBoxStyle.Exclamation)
                        r_read.Close()
                        connection.Close()
                        Return False
                    End Try

                Else

                    Dim spp = Split(r_read.GetValue(2), ",")
                    For i = 0 To spp.Length - 1
                        If valore = spp(i) Then
                            r_read.Close()
                            connection.Close()
                            Return True
                        End If
                    Next
                    MsgBox("Valore per il parametro (" & parametro & ") sbagliato, atteso (" & r_read.GetValue(2) & ")", MsgBoxStyle.Exclamation)
                    r_read.Close()
                    connection.Close()
                    Return False
                End If

            End If
            r_read.Close()
            connection.Close()
            Return True
        End While
        r_read.Close()
        connection.Close()
        Return False

    End Function
    Public Shared Function controlla_stampo(ByVal valore As String, ByVal macchina As String)
        Dim command_Master As New SqlClient.SqlCommand
        Dim Adapter_Master As New SqlClient.SqlDataAdapter
        Dim connection As New SqlClient.SqlConnection(connectionString)
        connection.Open()
        Dim sql_read = " SELECT TOP (1000) [mdc_machine]      ,[Stampo] " &
                         " FROM [dbo].[_PRODUZIONE_descrizione_macchine_stampi] WITH(NOLOCK)  " &
                         " where mdc_machine='" & macchina & "' and stampo='" & valore & "'"
        Dim command_Master_read = New SqlClient.SqlCommand(sql_read, connection)
        Dim r_read As SqlClient.SqlDataReader = command_Master_read.ExecuteReader()
        While (r_read.Read())
            r_read.Close()
            connection.Close()
            Return True
        End While
        r_read.Close()
        connection.Close()
        MsgBox("Numero Stampo errato", MsgBoxStyle.Exclamation)
        Return False
    End Function
    Public Shared Function controlla_programma_stampo(ByVal programma As String, ByVal macchina As String, ByVal stampo As String)
        Dim Adapter_Master As New SqlClient.SqlDataAdapter
        Dim connection As New SqlClient.SqlConnection(connectionString)
        connection.Open()
        Dim count As Integer = 0
        Try
            Dim sql_read As String = "SELECT COUNT(p.[Stampo])" & vbCrLf &
            "  FROM [dbo].[_PRODUZIONE_descrizione_macchine_stampi_parametri] p WITH(NOLOCK)" & vbCrLf &
            "  INNER JOIN dbo._PRODUZIONE_descrizione_macchine_stampi s WITH(NOLOCK)" & vbCrLf &
            "  on p.Stampo = s.Stampo" & vbCrLf &
            "  where s.mdc_machine = '" & macchina & "' and p.Stampo = '" & stampo & "' and (p.Programma LIKE REPLACE('" & programma & "',' ','%')" & vbCrLf &
            "or (SELECT count(value) FROM STRING_SPLIT(p.Programma, ',') WHERE value = '" & programma & "')>=1)"
            Dim command_Master_read = New SqlClient.SqlCommand(sql_read, connection)
            Dim r_read As SqlClient.SqlDataReader = command_Master_read.ExecuteReader()

            While (r_read.Read())
                count = r_read(0)
            End While
            r_read.Close()
            connection.Close()
        Catch ex As Exception
            connection.Close()
            MsgBox("Errore corrispondenza tra stampo (" & stampo & ") e programma (" & programma & ") per la macchina " & macchina, MsgBoxStyle.Exclamation)
            Return False
        End Try

        If count <= 0 Then
            MsgBox("Errore corrispondenza tra stampo (" & stampo & ") e programma (" & programma & ") per la macchina " & macchina, MsgBoxStyle.Exclamation)
            Return False
        Else
            Return True
        End If
    End Function
    Public Shared Function getnewarea(newmacch As String, PhaseString As String, Area As String)
        Try
            Using connection As New SqlConnection(connectionString)
                ' Apri la connessione
                connection.Open()

                ' Crea un oggetto SqlCommand per chiamare la stored procedure
                Using command As New SqlCommand("dbo.fn_switch_area", connection)
                    ' Imposta il tipo di comando come stored procedure
                    command.CommandType = CommandType.StoredProcedure

                    ' Aggiungi i parametri necessari
                    command.Parameters.Add("@mdc_machine", SqlDbType.VarChar, 50).Value = newmacch ' Sostituisci 50 con la dimensione appropriata
                    command.Parameters.Add("@PhaseString", SqlDbType.VarChar, 50).Value = PhaseString ' Sostituisci 50 con la dimensione appropriata
                    command.Parameters.Add("@OldNo", SqlDbType.VarChar, 50).Value = Area ' Sostituisci 50 con la dimensione appropriata
                    command.Parameters.Add("@Result", SqlDbType.VarChar, 255).Direction = ParameterDirection.Output ' Sostituisci 255 con la dimensione appropriata

                    ' Esegui la stored procedure
                    command.ExecuteNonQuery()

                    ' Ottieni il risultato dalla variabile di output
                    Dim risultato As String = Convert.ToString(command.Parameters("@Result").Value)

                    ' Usa il risultato come desiderato
                    Return risultato
                End Using
            End Using
        Catch ex As Exception

        End Try


    End Function
    Public Shared Function controlla_macchine_live_tesar(ByVal phasestring As String, macchina As String, sessiondev As Boolean) As String

        Dim sql As String
        sql = "SELECT COUNT([Mdc_Machine]) AS Ck" & vbCrLf &
                "FROM [Db_Nav_Prod].[dbo].[_TESAR_Macchine_Live] ml WITH(NOLOCK)" & vbCrLf &
                "     INNER JOIN [dbo].[Medacta CSP$Prod_ Ord_ Rout_ Alt_ Type] ma WITH(NOLOCK) ON ml.Mdc_Machine = ma.No_ COLLATE database_default" & vbCrLf &
                "     INNER JOIN [dbo].[Medacta CSP$Prod_ Order Rtng Line Export_originale] ex WITH(NOLOCK) ON ex.[Prod_ Order No_] = ma.[Prod_ Order No_]" & vbCrLf &
                "                                                                                              AND ex.[Routing Reference No_] = ma.[Routing Reference No_]" & vbCrLf &
                "                                                                                              AND ex.[Operation No_] = ma.[Operation No_]" & vbCrLf &
                "WHERE ml.Attiva = 1" & vbCrLf &
                "      AND ex.[Phase String] = '" & phasestring & "'" & vbCrLf &
                "      AND ml.Mdc_Machine = '" & macchina & "'"
        Dim ck As Int32 = readrecord(sql, 0)
        Return ck

    End Function
    Public Shared Function Aggiungi_Scarti_Tesar(ordine_tesar As String, operation_no As String, macchina As String, codiceoperatoretesar As String, articolo As String, tiposcarto As String, qtascarto As Int32, codicescarto As String, sessiondev As Boolean) As Boolean
        Return Wscarti.inserisci_scarti(ordine_tesar, operation_no, macchina, codiceoperatoretesar, articolo, tiposcarto, qtascarto, codicescarto, sessiondev)

    End Function


    Public Shared Function verifica_uid(uid As String, lotto As String, articolo As String)
        Dim str As String
        Dim lng As Int32 = Len(uid) / 2

        Dim a = Mid(uid, 3, lng - 2)
        Dim b = Mid(uid, lng + 2, lng - 2)
        a = Mid(a, 1, Len(a) - 1)
        b = Mid(b, 1, Len(b) - 1)

        str = a & b
        Dim sql As String
        sql = "SELECT count([RFID Uid]) " & vbCrLf &
               " FROM [RFID].[dbo].[RFID Uid] WITH (NOLOCK)" & vbCrLf &
               " WHERE [RFID Uid] = '" & str & "' and [Lot No_]='" & lotto & "' and REPLACE([Item No_], char(160), '')='" & articolo.Replace("VAL.", "") & "'"
        Dim cnt As Int16 = readrecord(sql, 0)
        If cnt = 0 Then
            Dim newstrrev = str.AsEnumerable.Reverse.ToArray
            Dim nstrrev As String = newstrrev(1) & newstrrev(0) & newstrrev(3) & newstrrev(2) & newstrrev(5) & newstrrev(4) & newstrrev(7) & newstrrev(6)
            sql = "SELECT count([RFID Uid]) " & vbCrLf &
               " FROM [RFID].[dbo].[RFID Uid] WITH (NOLOCK)" & vbCrLf &
               " WHERE [RFID Uid] = '" & nstrrev & "' and [Lot No_]='" & lotto & "' and REPLACE([Item No_], char(160), '')='" & articolo.Replace("VAL.", "") & "'"
            cnt = readrecord(sql, 0)
            Return cnt <> 0

        Else
            Return True
        End If

    End Function
    Public Shared Function decodifica_rfid(uid As String)
        Dim lng As Int32 = Len(uid) / 2
        Dim a = Mid(uid, 3, lng - 2)
        Dim b = Mid(uid, lng + 2, lng - 2)
        a = Mid(a, 1, Len(a) - 1)
        b = Mid(b, 1, Len(b) - 1)
        Return a & b
    End Function
    Public Shared Function verifica_Num_Stampe_RFID(lotto As String, articolo As String) As Short
        Try
            Dim sql As String
            sql = "SELECT count([RFID Uid]) " & vbCrLf &
               " FROM [RFID].[dbo].[RFID Uid] WITH (NOLOCK)" & vbCrLf &
               " WHERE [Lot No_]='" & lotto & "' and REPLACE([Item No_], char(160), '')='" & articolo.Replace("VAL.", "") & "'"
            Dim cnt As Int16 = readrecord(sql, 0)
            Return cnt
        Catch
            Return 0
        End Try

    End Function

    Public Shared Function regnavrecord(phasestring As String, dtiniz As DateTime, dtfin As DateTime, Usrnm As String, mdcmachine As String, qty As Int32, finished As Int32, scrap_qty As Int32, dev As Boolean, NO As String, idconn As Int32)
        Try

            If dev Then

                Select Case idconn
                    Case 2 'BC
                        For i = 1 To 2

                            Using client As New WsprogressImportTestBC.WebAvanzamentiProduzione_Service
                                ' client.UseDefaultCredentials = True
                                Dim myCred = New NetworkCredential("MEDACTA_CH\TesarLinkedSrv", "b6JbuLTeZES1H$3GkyvP")
                                client.UseDefaultCredentials = False
                                client.Credentials = myCred
                                Dim ws As New WsprogressImportTestBC.WebAvanzamentiProduzione With {
                                    .Phase_String = phasestring,
                                    .Posting_DateSpecified = True,
                                    .Posting_Date = dtiniz.ToString("yyyy-MM-dd"),
                                    .Output_QuantitySpecified = True,
                                    .Output_Quantity = qty,
                                    .Scrap_QuantitySpecified = True,
                                    .Scrap_Quantity = scrap_qty,
                                    .Starting_TimeSpecified = True,
                                    .Starting_Time = dtiniz.ToString("HH:mm:ss.fff"),
                                    .Ending_TimeSpecified = True,
                                    .Ending_Time = dtfin.ToString("HH:mm:ss.fff"),
                                    .User_Name = Usrnm, 'CognomeTextBox.Text
                                    .User_Insert = My.User.Name,
                                    .Reason_Code = 4,
                                    .Labour_TypeSpecified = True,
                                    .Labour_Type = 2,
                                    .FinishedSpecified = True,
                                    .Finished = finished,
                                    .Mdc_Machine = mdcmachine,
                                    .Insert_Type = 0,
                                    .Insert_TypeSpecified = True,
                                    .No = NO
                                }
                                client.Create(ws)
                                client.Dispose()
                            End Using
                            getConfig(sessiondev, idconn)
                            Dim sql As String
                            sql = "SELECT TOP (1) COUNT([Phase String])" & vbCrLf &
                                    "FROM [Medacta CSP$Prod_ Order Progress Import] with (NOLOCK)" & vbCrLf &
                                    "WHERE [Phase String] = '" & phasestring & "'"
                            Dim cnt As Int32 = readrecord(sql, 0)
                            If cnt > 0 Then
                                Return True
                            Else
                                If i = 1 Then
                                    Thread.Sleep(5000)
                                End If
                            End If
                        Next
                        Return False
                    Case 1 'Validation
                        For i = 1 To 2
                            Using client As New WsprogressImportTest.WebAvanzamentiProduzione_Service
                                ' client.UseDefaultCredentials = True
                                Dim myCred = New NetworkCredential("MEDACTA_CH\TesarLinkedSrv", "b6JbuLTeZES1H$3GkyvP")
                                client.UseDefaultCredentials = False
                                client.Credentials = myCred
                                Dim ws As New WsprogressImportTest.WebAvanzamentiProduzione With {
                                    .Phase_String = phasestring,
                                    .Posting_DateSpecified = True,
                                    .Posting_Date = dtiniz.ToString("yyyy-MM-dd"),
                                    .Output_QuantitySpecified = True,
                                    .Output_Quantity = qty,
                                    .Scrap_QuantitySpecified = True,
                                    .Scrap_Quantity = scrap_qty,
                                    .Starting_TimeSpecified = True,
                                    .Starting_Time = dtiniz.ToString("HH:mm:ss.fff"),
                                    .Ending_TimeSpecified = True,
                                    .Ending_Time = dtfin.ToString("HH:mm:ss.fff"),
                                    .User_Name = Usrnm, 'CognomeTextBox.Text
                                    .User_Insert = My.User.Name,
                                    .Reason_Code = 4,
                                    .Labour_TypeSpecified = True,
                                    .Labour_Type = 2,
                                    .FinishedSpecified = True,
                                    .Finished = finished,
                                    .Mdc_Machine = mdcmachine,
                                    .Insert_Type = 0,
                                    .Insert_TypeSpecified = True,
                                    .No = NO
                                }
                                client.Create(ws)
                                client.Dispose()
                            End Using
                            getConfig(sessiondev, idconn)
                            Dim sql As String
                            sql = "SELECT TOP (1) COUNT([Phase String])" & vbCrLf &
                                    "FROM [Medacta CSP$Prod_ Order Progress Import] with (NOLOCK)" & vbCrLf &
                                    "WHERE [Phase String] = '" & phasestring & "'"
                            Dim cnt As Int32 = readrecord(sql, 0)
                            If cnt > 0 Then
                                Return True
                            Else
                                If i = 1 Then
                                    Thread.Sleep(5000)
                                End If
                            End If
                        Next
                        Return False
                End Select

            Else
                For i = 1 To 2
                    Using client As New WsprogressImport.WebAvanzamentiProduzione_Service
                        ' client.UseDefaultCredentials = True
                        Dim myCred = New NetworkCredential("MEDACTA_CH\TesarLinkedSrv", "b6JbuLTeZES1H$3GkyvP")
                        client.UseDefaultCredentials = False
                        client.Credentials = myCred
                        Dim ws As WsprogressImport.WebAvanzamentiProduzione
                        ws = New WsprogressImport.WebAvanzamentiProduzione
                        ws.Phase_String = phasestring
                        ws.Posting_DateSpecified = True
                        ws.Posting_Date = dtiniz.ToString("yyyy-MM-dd")
                        ws.Output_QuantitySpecified = True
                        ws.Output_Quantity = qty
                        ws.Scrap_QuantitySpecified = True
                        ws.Scrap_Quantity = scrap_qty
                        ws.Starting_TimeSpecified = True
                        ws.Starting_Time = dtiniz.ToString("HH:mm:ss.fff")
                        ws.Ending_TimeSpecified = True
                        ws.Ending_Time = dtfin.ToString("HH:mm:ss.fff")
                        ws.User_Name = Usrnm 'CognomeTextBox.Text
                        ws.User_Insert = My.User.Name
                        ws.Reason_Code = 4
                        ws.Labour_TypeSpecified = True
                        ws.Labour_Type = 2
                        ws.FinishedSpecified = True
                        ws.Finished = finished
                        ws.Mdc_Machine = mdcmachine
                        ws.Insert_Type = 0
                        ws.Insert_TypeSpecified = True
                        ws.No = NO
                        client.Create(ws)
                        client.Dispose()
                    End Using
                    getConfig(sessiondev, idconn)
                    Dim sql As String
                    sql = "SELECT TOP (1) COUNT([Phase String])" & vbCrLf &
                            "FROM [Medacta CSP$Prod_ Order Progress Import] with (NOLOCK)" & vbCrLf &
                            "WHERE [Phase String] = '" & phasestring & "'"
                    Dim cnt As Int32 = readrecord(sql, 0)
                    If cnt > 0 Then
                        Return True
                    Else
                        If i = 1 Then
                            Thread.Sleep(5000)
                        End If
                    End If
                Next
                Return False
            End If

        Catch ex As Exception
            MsgBox("" & Err.Description)
            Return False
        End Try

    End Function
    Public Shared Function write_upload_error(ProdOrderNo As String, LineNo As Int32, OperationNo As String, Dir As String, FileName As String, Tipo As String, ls As List(Of String), IDDocumento As String, NumeroPezzo As String, idreport As String, descrizione As String, wrk As String, errore As String)
        Dim strSql As String
        strSql = "INSERT INTO [dbo].[_IBM_Upload_Error]" & vbCrLf &
                    "           ([ProdOrderNo]" & vbCrLf &
                    "           ,[LineNo]" & vbCrLf &
                    "           ,[OperationNo]" & vbCrLf &
                    "           ,[Dir]" & vbCrLf &
                    "           ,[FileName]" & vbCrLf &
                    "           ,[Tipo]" & vbCrLf &
                    "           ,[Data]" & vbCrLf &
                    "           ,[Stato],[Wrk],[Errore])" & vbCrLf &
                    "     VALUES" & vbCrLf &
                    "           ('" & ProdOrderNo & "'" & vbCrLf &
                    "           ," & LineNo & vbCrLf &
                    "		   ,'" & OperationNo & "'" & vbCrLf &
                    "           ,'" & Dir & "'" & vbCrLf &
                    "           ,'" & FileName & "'" & vbCrLf &
                    "           ,'" & Tipo & "'" & vbCrLf &
                    "           ,getdate()" & vbCrLf &
                    "           ,0,'" & wrk & "','" & errore & "')"
        Execute(strSql)

        For Each l In ls
            strSql = "INSERT INTO [dbo].[_IBM_Upload_Error_Ls]" & vbCrLf &
                "([ProdOrderNo], " & vbCrLf &
                " [LineNo], " & vbCrLf &
                " [OperationNo], " & vbCrLf &
                " [Ls]," & vbCrLf &
                " [FileName]" & vbCrLf &
                ")" & vbCrLf &
                "VALUES" & vbCrLf &
                "('" & ProdOrderNo & "', " & LineNo & "," & vbCrLf &
                " '" & OperationNo & "', " & vbCrLf &
                " '" & l.ToString & "'," & vbCrLf &
                 " '" & FileName & "'" & vbCrLf &
                ");"
            Execute(strSql)
        Next
        If IDDocumento <> "Test" Then
            strSql = "INSERT INTO [dbo].[Medacta CSP$Quote_Allegati]" & vbCrLf &
                                                    "([ID Documento], " & vbCrLf &
                                                    " [ID IBM], " & vbCrLf &
                                                    " [Numero Pezzo]," & vbCrLf &
                                                    " [Tipo]," & vbCrLf &
                                                    " [Descrizione]" & vbCrLf &
                                                    ")" & vbCrLf &
                                                    "VALUES" & vbCrLf &
                                                    "('" & UCase(IDDocumento) & "'" & vbCrLf &
                                                    " ,'FileInCoda'" & vbCrLf &
                                                    " ,'" & NumeroPezzo & "'" & vbCrLf &
                                                    " ," & idreport & vbCrLf &
                                                    " ,'" & descrizione & "'" & vbCrLf &
                                                    ")"

            Execute(strSql)
        End If

    End Function
    Public Shared Function assegna_ic_nav(idoperazione As String, Referenza As String, warning As Int32) As DataTable

        Dim Ds As New DataSet
        Dim sql As String
        Dim sp = Split(idoperazione, "/")
        Dim ordine As String = sp(0)
        Dim riga As Int32 = sp(1)
        Dim Operazione As String = sp(2)
        Try

            getConfig(sessiondev, idconn)
            sql = "SELECT TOP (1) [Draw]" & vbCrLf &
                    "FROM [dbo].[Medacta CSP$Prod_ Order Rtng Line Export]" & vbCrLf &
                    "WHERE [Prod_ Order No_] = '" & ordine & "'" & vbCrLf &
                    "      AND [Routing Reference No_] =" & riga

            Dim disegno As String = readrecord(sql, 0)
            sql = "select Revision from [Medacta CSP$Prod_ Order Line Design] where [Prod_ Order No_]='" & ordine & "' and [Line No_]=" & riga & " and Design='" & disegno & "'"
            Dim rev As Int32 = readrecord(sql, 0)
            sql = "SELECT TOP (1) dbo._CQ_StoricoReferenzeAbbinate.Revisione AS NumeroRevisioneDisegno, " & vbCrLf &
                    "               dbo._CQ_CAStorico.NumeroICS, " & vbCrLf &
                    "               dbo._CQ_CAStorico.Revisione, " & vbCrLf &
                    "               dbo._CQ_CAStorico.Nome_File, " & vbCrLf &
                    "               dbo._CQ_CAStorico.TipoDocumento, " & vbCrLf &
                    "               dbo._CQ_CAStorico.ID, " & vbCrLf &
                    "               dbo._CQ_StoricoReferenzeAbbinate.NumeroReferenza, " & vbCrLf &
                    "               derivedtbl_1.Operazione, " & vbCrLf &
                    "               dbo._CQ_CAStorico.[Etichetta Documento]," & vbCrLf &
                    "               '" & disegno & "' as Disegno" & vbCrLf &
                    "FROM dbo._CQ_CAStorico" & vbCrLf &
                    "     INNER JOIN dbo._CQ_StoricoReferenzeAbbinate ON dbo._CQ_CAStorico.ID = dbo._CQ_StoricoReferenzeAbbinate.idcastorico" & vbCrLf &
                    "     INNER JOIN" & vbCrLf &
                    "(" & vbCrLf &
                    "    SELECT Operazione, " & vbCrLf &
                    "           Referenza, " & vbCrLf &
                    "           idcastorico" & vbCrLf &
                    "    FROM dbo._CQ_CAQuoteStorico" & vbCrLf &
                    "    GROUP BY Operazione, " & vbCrLf &
                    "             Referenza, " & vbCrLf &
                    "             idcastorico" & vbCrLf &
                    "    UNION" & vbCrLf &
                    "    SELECT Operazione," & vbCrLf &
                    "           CASE" & vbCrLf &
                    "               WHEN Referenza IS NULL" & vbCrLf &
                    "                    OR Referenza = ''" & vbCrLf &
                    "               THEN '" & Referenza & "'" & vbCrLf &
                    "               ELSE Referenza" & vbCrLf &
                    "           END AS Referenza, " & vbCrLf &
                    "           idcastorico" & vbCrLf &
                    "    FROM dbo._CQ_CAFunzionaliStorico" & vbCrLf &
                    "    GROUP BY Operazione, " & vbCrLf &
                    "             Referenza, " & vbCrLf &
                    "             idcastorico" & vbCrLf &
                    ") AS derivedtbl_1 ON dbo._CQ_StoricoReferenzeAbbinate.idcastorico = derivedtbl_1.idcastorico" & vbCrLf &
                    "                     AND dbo._CQ_StoricoReferenzeAbbinate.NumeroReferenza = derivedtbl_1.Referenza" & vbCrLf &
                    "WHERE(dbo._CQ_CAStorico.TipoDocumento IN('ICS', 'ICIM'))" & vbCrLf &
                    "     AND (dbo._CQ_CAStorico.ICSProduzione = 1)" & vbCrLf &
                    "     AND (dbo._CQ_StoricoReferenzeAbbinate.NumeroReferenza = '" & Referenza & "')" & vbCrLf &
                    "     AND (dbo._CQ_StoricoReferenzeAbbinate.Revisione =" & rev & ")" & vbCrLf &
                    "     AND (dbo._CQ_CAStorico.ICSApprovata = 1)" & vbCrLf &
                    "     AND (dbo._CQ_CAStorico.Obsoleta = 0)" & vbCrLf &
                    "     AND (derivedtbl_1.Operazione = " & Operazione & ")" & vbCrLf &
                    "ORDER BY dbo._CQ_CAStorico.ID DESC;"


            OpenRecordset(sql, "rev_ic", Ds)

            Dim a = Ds.Tables("rev_ic").Rows.Count
            If warning = 0 Then
                If a = 0 Then
                    MsgBox("Operazione Annullata, IC non trovata !!", MsgBoxStyle.Exclamation)
                    Return Ds.Tables("rev_ic")
                End If
            Else
                If a = 0 Then
                    Return Ds.Tables("rev_ic")
                End If
            End If


            sql = "UPDATE [dbo].[Medacta CSP$Prod_ Order Line Design]" & vbCrLf &
                            "  SET " & vbCrLf &
                            "      [ID Document Control] =" & Ds.Tables("rev_ic").Rows(0).Item("ID") & ",[ID Document Control Operation]='" & Operazione & "'" & vbCrLf &
                            "WHERE [Prod_ Order No_] = '" & ordine & "'" & vbCrLf &
                            "      AND [Line No_] =" & riga & " and Design='" & disegno & "'"
            getConfig(sessiondev, idconn)
            Dim ckupdt As Int32 = Execute(sql)
            If warning = 0 Then
                If ckupdt <> 1 Then
                    MsgBox("Operazione Annullata, Aggiornamento CA non riuscito !!", MsgBoxStyle.Exclamation)
                    Ds.Tables("rev_ic").Clear()
                End If
            Else
                If ckupdt <> 1 Then
                    Ds.Tables("rev_ic").Clear()
                End If

            End If
            Return Ds.Tables("rev_ic")
        Catch ex As Exception
            MsgBox("" & Err.Description)
        End Try
    End Function

    Public Shared Function controlla_operazioni_precedenti_non_chiuse(ByVal lotto As String, ByVal ordine As String, ByVal operazione As Int32, tipo As Int32, idoperazione As String)
        'Try
        '    Dim connection As SqlConnection
        '    Dim command_Master As SqlCommand
        '    Dim Adapter_Master As New SqlDataAdapter
        '    connection = New SqlConnection(connectionString)
        '    connection.Open()
        '    Dim sql_Operatori

        '    Select Case tipo
        '        Case Is = 5
        '            Dim sp = Split(idoperazione, "/")
        '            sql_Operatori = "SELECT ex.[Operation No_], " & vbCrLf &
        '                                "       ex.Description, " & vbCrLf &
        '                                "       ex.[Standard Task Code], " & vbCrLf &
        '                                "       ex.[Import Date Time], " & vbCrLf &
        '                                "(" & vbCrLf &
        '                                "    SELECT TOP 1 Finished" & vbCrLf &
        '                                "    FROM [Medacta CSP$Prod_ Order Progress Import]" & vbCrLf &
        '                                "    WHERE [Phase String] = ex.[Phase String]" & vbCrLf &
        '                                "          AND Finished = 1" & vbCrLf &
        '                                "    ORDER BY [Import Date Time] DESC) AS Finished" & vbCrLf &
        '                                "FROM [Medacta CSP$Prod_ Order Rtng Line Export] ex" & vbCrLf &
        '                                "WHERE CAST(ex.[Operation No_] AS INT) < " & CInt(sp(2)) & vbCrLf &
        '                                "      AND ex.[Prod_ Order No_] = '" & ordine & "'" & vbCrLf &
        '                                "      AND ex.[Lot No_] = '" & lotto & "'" & vbCrLf &
        '                                "      AND ex.No_ IN('01-TORNITURA', '01-FRESATURA', '04-TORNITURA', '04-FRESATURA') and ex.[Canceled Date]<'2000-01-01' and ex.[Canceled by User]=''"
        '        Case Else

        '            sql_Operatori = " SELECT        TOP (3) CAST([Operation No_] AS int) AS [Operation No_], Description, [Lot No_] " &
        '                                        " FROM            dbo.[Medacta CSP$Prod_ Order Rtng Line Export_originale] AS [Medacta CSP$Prod_ Order Rtng Line Export_originale_1] " &
        '                                        " WHERE        ([Import Date Time] = CONVERT(DATETIME, '1753-01-01 00:00:00', 102)) AND (NOT (No_ LIKE N'03-%')) AND ([Prod_ Order No_] = N'" & ordine & "') AND   " &
        '                                        "                         (CAST([Operation No_] AS int) < " & operazione & ") AND (CAST([Operation No_] AS int) > 090) AND ([Lot No_] = N'" & lotto & "')  " &
        '                                        " ORDER BY IDRK DESC  "
        '    End Select

        '    command_Master = New SqlCommand(sql_Operatori, connection)
        '    Adapter_Master.SelectCommand = command_Master
        '    Dim rd = command_Master.ExecuteReader
        '    Dim list As New List(Of String)
        '    list.Clear()
        '    Dim blocca As Boolean = False
        '    While rd.Read()
        '        If tipo = 5 Then
        '            If IsDBNull(rd.GetValue(4)) Then
        '                list.Add(rd.GetValue(0) & " " & rd.GetValue(1))
        '                blocca = True
        '            End If

        '        Else
        '            list.Add(rd.GetValue(0) & " " & rd.GetValue(1))
        '        End If
        '    End While
        '    rd.Close()
        '    connection.Close()
        '    If list.Count <> 0 Then
        '        If (tipo = 5) And blocca = True Then

        '            Dim Message As String = "Attenzione!!!!, le seguenti operazioni non sono state rilasciate!!! " & Environment.NewLine & String.Join(Environment.NewLine, list.ToArray()) & vbCrLf & "Continuare con il codice di sblocco ?"
        '            Dim ms As DialogResult = MessageBox.Show(Message, "Avviso", MessageBoxButtons.OKCancel, MessageBoxIcon.Error)
        '            If ms = DialogResult.Cancel Then
        '                Return False
        '            End If
        '            Try
        '                Dim cod As Int32 = InputBox("Inserisci codice di sblocco")
        '                If cod = 142027 Then
        '                    Return True
        '                Else
        '                    MsgBox("Codice Errato", MsgBoxStyle.Exclamation)
        '                    Return False
        '                End If
        '            Catch ex As Exception
        '                Return False
        '            End Try
        '        Else
        '            Dim message = "Attenzione, le seguenti operazioni non sono state rilasciate!!!" & Environment.NewLine & String.Join(Environment.NewLine, list.ToArray()) & Environment.NewLine & "Continuare ?"
        '            Dim ms As DialogResult = MessageBox.Show(message, "Avviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        '            If ms = Windows.Forms.DialogResult.No Then
        '                Return False
        '            End If
        '        End If
        '    End If
        '    Return True
        'Catch ex As Exception
        '    MsgBox("" & Err.Description)
        'End Try
        Try
            Dim list As New List(Of String)
            Dim blocca As Boolean = False
            Dim sp = Split(idoperazione, "/")
            getConfig(sessiondev, idconn)
            Dim sqlCkMandatory As String = ""
            sqlCkMandatory = "SELECT TOP (1) e.[Operation No_] + ' ' + e.Description , " &
                 "               [Import Date Time]" &
                 " FROM [Medacta CSP$Prod_ Order Rtng Line Export_originale] e" &
                 "     INNER JOIN [Medacta CSP$Standard Task] s ON e.[Standard Task Code] = s.Code" &
                 " WHERE e.[Prod_ Order No_] = '" & sp(0) & "'" &
                 "      AND e.[Routing Reference No_] = " & sp(1) &
                 "      AND s.[Mandatory Task Code] = 1" &
                 "      AND [Canceled Date] < '2000-01-01'" &
                 "      AND [Import Date Time] = '1753-01-01' AND cast(e.[Operation No_] as int)<" & operazione
            Dim result As String = readrecord(sqlCkMandatory, 0)
            If result <> "" Then
                MsgBox("Operazione annullata!" & vbCrLf & "Fase con Mandatory Task Code non completata (" & result & ") ", MsgBoxStyle.Critical)
                blocca = True
                Return False
            End If
            Using connection As New SqlConnection(connectionString)
                connection.Open()

                Dim sql_Operatori As String
                If tipo = 5 Then
                    sql_Operatori = "
                SELECT ex.[Operation No_], ex.Description, ex.[Standard Task Code], ex.[Import Date Time],
                (SELECT TOP 1 Finished
                 FROM [Medacta CSP$Prod_ Order Progress Import]
                 WHERE [Phase String] = ex.[Phase String] AND Finished = 1
                 ORDER BY [Import Date Time] DESC) AS Finished
                 FROM [Medacta CSP$Prod_ Order Rtng Line Export] ex
                 WHERE CAST(ex.[Operation No_] AS INT) < @OperationNumber
                 AND ex.[Prod_ Order No_] = @Ordine
                 AND ex.[Lot No_] = @Lotto
                 AND ex.No_ IN('01-TORNITURA', '01-FRESATURA', '04-TORNITURA', '04-FRESATURA')
                 AND ex.[Canceled Date]<'2000-01-01'
                 AND ex.[Canceled by User]=''"
                Else
                    sql_Operatori = "
                 SELECT TOP 3 CAST([Operation No_] AS int) AS [Operation No_], Description, [Lot No_], 
				 (SELECT TOP 1 Finished
                 FROM [Medacta CSP$Prod_ Order Progress Import]
                 WHERE [Phase String] = ex.[Phase String] AND Finished = 1
                 ORDER BY [Import Date Time] DESC) AS Finished 
                 FROM dbo.[Medacta CSP$Prod_ Order Rtng Line Export_originale] ex
                 WHERE [Import Date Time] = CONVERT(DATETIME, '1753-01-01 00:00:00', 102)
                 AND NOT (No_ LIKE N'03-%')
                 AND [Prod_ Order No_] = @Ordine
                 AND CAST([Operation No_] AS int) < @Operazione
                 AND CAST([Operation No_] AS int) > 090
                 AND [Lot No_] = @Lotto
                 ORDER BY IDRK DESC "
                End If

                Using command_Master As New SqlCommand(sql_Operatori, connection)
                    If tipo = 5 Then
                        command_Master.Parameters.AddWithValue("@OperationNumber", CInt(sp(2)))
                    End If
                    command_Master.Parameters.AddWithValue("@Ordine", ordine)
                    command_Master.Parameters.AddWithValue("@Lotto", lotto)
                    If tipo <> 5 Then
                        command_Master.Parameters.AddWithValue("@Operazione", operazione)
                    End If

                    Using rd = command_Master.ExecuteReader()
                        While rd.Read()
                            If tipo = 5 AndAlso IsDBNull(rd("Finished")) Then
                                list.Add($"{rd("Operation No_")} {rd("Description")}")
                                blocca = True
                            Else
                                If IsDBNull(rd("Finished")) Then
                                    list.Add($"{rd("Operation No_")} {rd("Description")}")
                                End If

                            End If
                        End While
                    End Using
                End Using
            End Using

            If list.Count > 0 Then
                Dim message As String
                If tipo = 5 AndAlso blocca Then
                    message = $"Attenzione!!!!, le seguenti operazioni non sono state rilasciate!!!{Environment.NewLine}{String.Join(Environment.NewLine, list)}{vbCrLf}Continuare con il codice di sblocco?"
                    If MessageBox.Show(message, "Avviso", MessageBoxButtons.OKCancel, MessageBoxIcon.Error) = DialogResult.Cancel Then Return False

                    Dim cod As Integer
                    If Integer.TryParse(InputBox("Inserisci codice di sblocco"), cod) AndAlso cod = 142027 Then
                        Return True
                    Else
                        MsgBox("Codice Errato", MsgBoxStyle.Exclamation)
                        Return False
                    End If
                Else
                    message = $"Attenzione, le seguenti operazioni non sono state rilasciate!!!{Environment.NewLine}{String.Join(Environment.NewLine, list)}{Environment.NewLine}Continuare?"
                    If MessageBox.Show(message, "Avviso", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.No Then Return False
                End If
            End If

            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try


    End Function

    Public Shared Function controlla_operazione_precedente(ordine As String, riga As Int32, operazione As String) As Boolean
        ' Return True
        Dim connection As SqlConnection
        Dim Adapter_Master As New SqlDataAdapter
        connection = New SqlConnection(connectionString)
        connection.Open()
        Try
            Dim sql As String
            sql = "SELECT [Prod_ Order No_], " & vbCrLf &
                    "       [Routing Reference No_], " & vbCrLf &
                    "       [Operation No_], " & vbCrLf &
                    "       [Previous Operation No_], " & vbCrLf &
                    "(" & vbCrLf &
                    "    SELECT [Operation No_]" & vbCrLf &
                    "    FROM [Medacta CSP$Prod_ Order Rtng Line Export] WITH(NOLOCK)" & vbCrLf &
                    "    WHERE [Prod_ Order No_] = pdl.[Prod_ Order No_]" & vbCrLf &
                    "          AND [Routing Reference No_] = pdl.[Routing Reference No_]" & vbCrLf &
                    "          AND [Operation No_] = pdl.[Previous Operation No_]" & vbCrLf &
                    "          AND No_ IN('01-TORNITURA', '01-FRESATURA', '04-TORNITURA', '04-FRESATURA') and [Canceled Date]<'2000-01-01' and [Canceled by User]=''" & vbCrLf &
                    ") AS [Previous Operation No_]" & vbCrLf &
                    "FROM dbo.[Medacta CSP$Prod_ Order Routing Line] pdl WITH(NOLOCK)" & vbCrLf &
                    "WHERE([Prod_ Order No_] = '" & ordine & "')" & vbCrLf &
                    "     AND ([Routing Reference No_] =" & riga & ")" & vbCrLf &
                    "     AND ([Operation No_] = '" & operazione & "')"

            Dim command_Master = New SqlCommand(sql, connection)
            Dim rd As SqlDataReader = command_Master.ExecuteReader()

            Dim lastop As String = ""
            While (rd.Read())
                If Not (IsDBNull(rd.GetValue(4))) Then
                    lastop = rd.GetValue(4)
                Else
                    lastop = ""
                End If

            End While
            rd.Close()
            command_Master.Dispose()
            connection.Close()
            If lastop <> "" Then

                Dim phaselastop As String = ordine & "/" & riga & "/" & lastop
                sql = "SELECT im.[Phase String]" & vbCrLf &
                        "FROM [Medacta CSP$Prod_ Order Progress Import] im WITH(NOLOCK)" & vbCrLf &
                        "     INNER JOIN [Medacta CSP$Prod_ Order Rtng Line Export] ex ON im.[Phase String] = ex.[Phase String]" & vbCrLf &
                        "WHERE im.[Phase String] = '" & phaselastop & "'" & vbCrLf &
                        "      AND ex.[Canceled Date] < '2000-01-01'" & vbCrLf &
                        "      AND ex.[Canceled by User] = ''" & vbCrLf &
                        "      AND Finished = 1 AND ex.No_ IN('01-TORNITURA', '01-FRESATURA', '04-TORNITURA', '04-FRESATURA')" & vbCrLf &
                        "UNION" & vbCrLf &
                        "SELECT ex.[Phase String]" & vbCrLf &
                        "FROM [Medacta CSP$Capacity Ledger Entry] cle WITH(NOLOCK)" & vbCrLf &
                        "     INNER JOIN [Medacta CSP$Prod_ Order Rtng Line Export] ex ON cle.[Prod_ Order No_] = ex.[Prod_ Order No_]" & vbCrLf &
                        "                                                                 AND cle.[Routing Reference No_] = ex.[Routing Reference No_]" & vbCrLf &
                        "                                                                 AND cle.[Operation No_] = ex.[Operation No_]" & vbCrLf &
                        "WHERE ex.[Phase String] = '" & phaselastop & "'" & vbCrLf &
                        "      AND ex.[Canceled Date] < '2000-01-01'" & vbCrLf &
                        "      AND ex.[Canceled by User] = ''" & vbCrLf &
                        "      AND cle.Subcontracting = 1" & vbCrLf &
                        "      AND [WIP Item Qty_] < 0;"
                connection.Open()
                command_Master = New SqlCommand(sql, connection)
                rd = command_Master.ExecuteReader()
                While (rd.Read())
                    rd.Close()
                    connection.Close()
                    Return True
                End While
                rd.Close()
                command_Master.Dispose()
                connection.Close()
                Return False
            End If
            Return True
        Catch ex As Exception
            MsgBox("" & ex.Message & vbCrLf & ex.StackTrace.ToString)
            Return False
        End Try
    End Function

    Public Shared Function Timbrature(str_badge As String) As DataTable
        Try
            Dim fl = New Altamira11.wslistatimbratureFilters
            Dim Badge = New Altamira11.wslistatimbratureFiltersPunchesNrBadge() {New Altamira11.wslistatimbratureFiltersPunchesNrBadge}
            Dim fldata = New Altamira11.wslistatimbratureData(0) {New Altamira11.wslistatimbratureData}
            Dim datain = New Altamira11.wslistatimbratureFiltersPunchesOriginalData(0) {New Altamira11.wslistatimbratureFiltersPunchesOriginalData}
            Dim data() As Altamira11.wslistatimbratureData
            Dim client = New Altamira11.MEDACTAINTERNATIONALSA
            Badge(0).Value = str_badge
            Badge(0).Comparison = Altamira11.Comparison.Contains
            fl.PunchesNrBadge = Badge
            Dim dtstartin As DateTime = Now.ToString("dd/MM/yyyy") & " 00:00:00"
            Dim dtstart As DateTime = DateAdd(DateInterval.Day, -3, dtstartin)
            datain(0).From = dtstart.ToString("dd/MM/yyyy HH:mm:ss")
            datain(0).FromSpecified = True
            datain(0).To = Now.ToString("dd/MM/yyyy 22:15:00")
            datain(0).ToSpecified = True
            datain(0).Comparison = Altamira11.Comparison.InsideInterval
            fl.PunchesOriginalData = datain
            data = client.wslistatimbrature("gfSDQ8h7YtfB3dUrWAdHDYN2Hq80klS6ptcAY7gbyVIY9luhhaghX5fM8hJ3Qsxx", Altamira11.Language.ITA, fl)
            Dim tmptable As New DataTable
            tmptable.Columns.Add("OPERAT")
            tmptable.Columns.Add("DATA")
            tmptable.Columns.Add("BADGE")
            For i = 0 To data.Length - 1
                Dim row As DataRow
                row = tmptable.NewRow
                row("OPERAT") = data(i).EmployeesSurnameAndName
                row("DATA") = data(i).PunchesOriginalData.ToString("yyyy-MM-dd HH:mm:ss")
                row("BADGE") = data(i).PunchesNrBadge
                tmptable.Rows.Add(row)
            Next
            Return tmptable
        Catch ex As Exception
            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
        End Try

    End Function
    Public Shared Function Timbrature_by_id(name As String, tipo As Int32) As DataTable
        Try
            Dim fl = New Altamira11.wslistatimbratureFilters
            Dim id = New Altamira11.wslistatimbratureFiltersEmployeesCustomerID(0) {New Altamira11.wslistatimbratureFiltersEmployeesCustomerID}
            Dim fldata = New Altamira11.wslistatimbratureData(0) {New Altamira11.wslistatimbratureData}
            Dim datain = New Altamira11.wslistatimbratureFiltersPunchesOriginalData(0) {New Altamira11.wslistatimbratureFiltersPunchesOriginalData}
            Dim data() As Altamira11.wslistatimbratureData
            Dim client = New Altamira11.MEDACTAINTERNATIONALSA
            If tipo = 1 Then
                id(0).Value = name
                id(0).Comparison = Altamira11.Comparison.Contains
                fl.EmployeesCustomerID = id

            End If

            Dim dtstartin As DateTime = Now.ToString("dd/MM/yyyy") & " 00:00:00"
            Dim dtstart As DateTime = DateAdd(DateInterval.Day, -1, dtstartin)
            datain(0).From = dtstart.ToString("dd/MM/yyyy HH:mm:ss")
            datain(0).FromSpecified = True
            datain(0).To = Now.ToString("dd/MM/yyyy 22:15:00")
            datain(0).ToSpecified = True
            datain(0).Comparison = Altamira11.Comparison.InsideInterval
            fl.PunchesOriginalData = datain
            data = client.wslistatimbrature("gfSDQ8h7YtfB3dUrWAdHDYN2Hq80klS6ptcAY7gbyVIY9luhhaghX5fM8hJ3Qsxx", Altamira11.Language.ITA, fl)

            Dim tmptable As New DataTable
            tmptable.Columns.Add("OPERAT")
            tmptable.Columns.Add("DATA")
            tmptable.Columns.Add("BADGE")
            If tipo = 0 Then
                Dim strSql As String
                strSql = "DELETE [dbo].[_TEMP_Badge_Altamira]"
                Try
                    Execute(strSql)
                Catch ex As Exception
                End Try
            End If
            For i = 0 To data.Length - 1
                If tipo = 1 Then
                    Dim row As DataRow
                    row = tmptable.NewRow
                    row("OPERAT") = data(i).EmployeesSurnameAndName
                    row("DATA") = data(i).PunchesOriginalData.ToString("yyyy-MM-dd HH:mm:ss")
                    row("BADGE") = data(i).PunchesNrBadge
                    tmptable.Rows.Add(row)
                Else
                    Dim strSql As String
                    strSql = "INSERT INTO [dbo].[_TEMP_Badge_Altamira]" & vbCrLf &
                                "           ([Nome],[Badge])" & vbCrLf &
                                "     VALUES" & vbCrLf &
                                "           ('" & data(i).EmployeesSurnameAndName & "'" & vbCrLf &
                                "           ,'" & data(i).PunchesNrBadge & "')"
                    Try
                        Execute(strSql)
                    Catch ex As Exception
                    End Try
                End If
            Next
            Return tmptable
        Catch ex As Exception
            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
        End Try

    End Function
    ''' <summary>
    ''' Read the app.config file of the project, assigning the values of DirBatchRecord and DatabaseSMG.
    ''' </summary>
    Public Shared Sub getConfig(sessiondev As Boolean, idconn As Int32)
        Try
            If sessiondev Then
                devMode(idconn)
            Else
                DatabaseSmg = System.Configuration.ConfigurationManager.AppSettings("DatabaseSMG")
                prodMode()
                dirbatchrecord = System.Configuration.ConfigurationManager.AppSettings("DirBatchRecord")
            End If

        Catch ex As Exception
            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
        End Try
    End Sub

    Public Shared Sub getConfigTesar()
        Try
            connectionString = System.Configuration.ConfigurationManager.AppSettings("MYConnection_db_tesar")
        Catch ex As Exception
            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
        End Try
    End Sub
    Public Shared Sub getConfigProdService()
        Try
            connectionString = System.Configuration.ConfigurationManager.AppSettings("MYConnection_ProdService")
        Catch ex As Exception
            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
        End Try
    End Sub
    Public Shared Sub getConfigProdService1()
        Try
            connectionString = System.Configuration.ConfigurationManager.AppSettings("MYConnection_ProdService_1")
        Catch ex As Exception
            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
        End Try
    End Sub
    Public Shared Sub getConfigProdServiceVar(connection As String)

        Try
            connectionString = System.Configuration.ConfigurationManager.AppSettings(connection)
        Catch ex As Exception
            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
        End Try
    End Sub
    Public Shared Sub getConfigDbPack()
        Try
            connectionString = System.Configuration.ConfigurationManager.AppSettings("MYConnection_DbPack")
        Catch ex As Exception
            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
        End Try
    End Sub



    ''' <summary>
    ''' Get the Operatori table from SQL to the dataset passed.
    ''' </summary>
    Public Shared Sub getOperatori(ByRef ds As DataSet)

        Dim strSQL As String = "select * from Operatori WITH(NOLOCK)"

        Try
            OpenRecordset(strSQL, "Operatori", ds)
        Catch ex As Exception

            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
        End Try

    End Sub
    ''' <summary>
    ''' Set connectionString and DatabaseNav the value of the developer server read from app.config
    ''' </summary>
    Public Shared Sub devMode(idconn As Int32)
        Try

            getConfig(False, idconn)
            Dim sql As String = " SELECT   [CONnectiONStringDev] + '|' +[DatabaseNav]  " & vbCrLf &
                                " FROM  [dbo].[_SMG_Setup_Db_Dev]" & vbCrLf &
                                " WHERE  id=" & idconn
            Dim sp = Split(readrecord(sql, 0), "|")

            connectionString = sp(0)
            DatabaseNav = sp(1)
            sessiondev = True
        Catch ex As Exception

            Throw New Exception("getConfig: " & Err.Description, ex)
        End Try
    End Sub
    ''' <summary>
    ''' Set connectionString and DatabaseNav the value of the Production server read from app.config
    ''' </summary>
    Public Shared Sub prodMode()
        Try
            connectionString = System.Configuration.ConfigurationManager.AppSettings("MYConnection_db_nav_2016")
            DatabaseNav = System.Configuration.ConfigurationManager.AppSettings("DatabaseNav2016")
            sessiondev = False
        Catch ex As Exception

            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
        End Try
    End Sub
    ''' <summary>
    ''' Set connectionString and DatabaseNav the value of the DB ETICHETTE read from app.config
    ''' </summary>
    Public Shared Sub dbEtichette()
        Try
            connectionString = System.Configuration.ConfigurationManager.AppSettings("MYConnection_db_conf")
            DatabaseNav = "db_etichette"
            sessiondev = False
        Catch ex As Exception
            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
        End Try
    End Sub
    ''' <summary>
    ''' Set connectionString and DatabaseNav the value of the DB ETICHETTE DEV read from app.config
    ''' </summary>
    Public Shared Sub dbEtichetteDEV()
        Try
            connectionString = System.Configuration.ConfigurationManager.AppSettings("MYConnection_db_conf_dev")
            DatabaseNav = "db_etichette_dev"
            sessiondev = True
        Catch ex As Exception
            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
        End Try
    End Sub
    ''' <summary>
    ''' Execute the SQL select statement passed and return a datatable; it is possible to define the table name, to pass a dataset to add the result in, and saving the SqlDataAdapter for using the UpdateDA method
    ''' </summary>
    Public Shared Function OpenRecordset(ByVal strSQL As String, Optional ByVal strTableName As String = "Table1", Optional ByRef ds As DataSet = Nothing, Optional ByVal bStoreDA As Boolean = False, Optional ByVal param As SqlParameterCollection = Nothing) As DataTable
        Dim Adapter As New SqlDataAdapter
        Dim connection As SqlConnection = Nothing
        Dim dt As DataTable
        Try

            'Apro la connessione
            connection = New SqlConnection(connectionString)
            connection.Open()
            'Instanzio il comando
            If bUseReadUncommitted Then
                strSQL = "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; " & vbCrLf & strSQL
            End If
            Dim command As New SqlCommand(strSQL, connection)
            command.CommandTimeout = 0

            If Not param Is Nothing Then
                For Each parTmp As SqlParameter In param
                    command.Parameters.Add(parTmp.ParameterName, parTmp.SqlDbType)
                    command.Parameters(parTmp.ParameterName).Value = parTmp.Value
                Next

            End If

            Adapter.SelectCommand = command

            'Se ho passato il nome, lo assegno al datatable e gestisco il relativo dataset
            If strTableName <> "" Then
                dt = New DataTable(strTableName)
                Adapter.Fill(dt)

                If ds Is Nothing Then
                    ds = New DataSet
                Else

                    If ds.Tables.Contains(strTableName) Then
                        ds.Tables.Remove(strTableName)
                    End If

                End If
                ds.AcceptChanges()
                ds.Tables.Add(dt)
                ds.AcceptChanges()

            Else
                dt = New DataTable
                Adapter.Fill(dt)
            End If

            dt.AcceptChanges()

            If bStoreDA Then

                If DataAdapterDictionary.ContainsKey(strTableName) Then
                    DataAdapterDictionary.Item(strTableName) = Adapter
                Else
                    DataAdapterDictionary.Add(strTableName, Adapter)
                End If

            End If

            Return dt

        Catch sqlEx As SqlException
            Select Case sqlEx.Number
                Case -1, 2, 53
                    MsgBox("Connection Failed! Messagge:" & vbCrLf & sqlEx.Message)

                Case Else
                    MsgBox("General SQL Error Messagge:" & vbCrLf & sqlEx.Message & vbCrLf & "Query:" & vbCrLf & strSQL & vbCrLf)
            End Select
            Return Nothing
        Catch ex As Exception

            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
            Return Nothing
        Finally
            If Not connection Is Nothing Then
                connection.Close()
            End If
        End Try
    End Function

    Public Shared Function cambia_permessi_Fold_ibm(ByVal os As IObjectStore, id As String, fornitore As Int32, UtetenteFornitore As String, GranteeType As String, AccessMask As Int32, InheritableDepth As Int32) As Boolean
        Dim Document As IFolder = CEUtil.FetchFolderByID(os, id)
        Dim IBMAccessPermissionList As IAccessPermissionList = Document.Permissions
        Dim IBMAccessPermission As IAccessPermission
        IBMAccessPermission = Factory.AccessPermission.CreateInstance
        Dim IBMAccessPermissionProperties As IProperties = IBMAccessPermission.Properties
        IBMAccessPermissionProperties("GranteeType") = GranteeType
        IBMAccessPermissionProperties("GranteeName") = UtetenteFornitore
        IBMAccessPermissionProperties("AccessMask") = AccessMask
        IBMAccessPermissionProperties("InheritableDepth") = InheritableDepth
        IBMAccessPermissionProperties("PermissionSource") = 1
        IBMAccessPermissionList.Add(IBMAccessPermission)
        Document.Save(RefreshMode.REFRESH)
    End Function
    Public Shared Function rimuovi_permessi_fold_ibm(ByVal os As IObjectStore, id As String, UtetenteFornitore As String, codicefornitore As Int32) As Boolean
        Dim Document As IFolder = CEUtil.FetchFolderByID(os, id)
        Dim IBMAccessPermissionList As IAccessPermissionList = Document.Permissions
        Dim IBMAccessPermission As IAccessPermission
        IBMAccessPermission = Factory.AccessPermission.CreateInstance

        For Each IBMAccessPermission In IBMAccessPermissionList
            Dim str As String = IBMAccessPermission.GranteeName
            If str.Contains(codicefornitore) Then
                Dim IBMAccessPermissionProperties As IProperties
                IBMAccessPermissionProperties = IBMAccessPermission.Properties
                IBMAccessPermissionList.Remove(IBMAccessPermission)
                Exit For
            End If
        Next
        For Each IBMAccessPermission In IBMAccessPermissionList
            Dim str As String = IBMAccessPermission.GranteeName
            If str.Contains("EXTOBJUsers") Then
                Dim IBMAccessPermissionProperties As IProperties
                IBMAccessPermissionProperties = IBMAccessPermission.Properties
                IBMAccessPermissionList.Remove(IBMAccessPermission)
                Exit For
            End If
        Next
        For Each IBMAccessPermission In IBMAccessPermissionList
            Dim str As String = IBMAccessPermission.GranteeName
            If str.Contains("OBJUsers") Then
                Dim IBMAccessPermissionProperties As IProperties
                IBMAccessPermissionProperties = IBMAccessPermission.Properties
                IBMAccessPermissionList.Remove(IBMAccessPermission)
                Exit For
            End If
        Next
        Document.Save(RefreshMode.REFRESH)
    End Function


    Public Shared Function cambia_permessi_doc_ibm(ByVal os As IObjectStore, id As String, fornitore As Int32, UtetenteFornitore As String, GranteeType As String, AccessMask As Int32, InheritableDepth As Int32) As Boolean
        Try
            Dim Document As IDocument = CEUtil.FetchDocByID(os, id)
            Dim IBMAccessPermissionList As IAccessPermissionList = Document.Permissions
            Dim IBMAccessPermission As IAccessPermission
            IBMAccessPermission = Factory.AccessPermission.CreateInstance
            Dim IBMAccessPermissionProperties As IProperties = IBMAccessPermission.Properties
            IBMAccessPermissionProperties("GranteeType") = GranteeType
            IBMAccessPermissionProperties("GranteeName") = UtetenteFornitore
            IBMAccessPermissionProperties("AccessMask") = AccessMask
            IBMAccessPermissionProperties("InheritableDepth") = InheritableDepth
            IBMAccessPermissionProperties("PermissionSource") = 1
            IBMAccessPermissionList.Add(IBMAccessPermission)
            Document.Save(RefreshMode.REFRESH)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function
    Public Shared Function rimuovi_permessi_doc_ibm(ByVal os As IObjectStore, id As String, UtetenteFornitore As String, codicefornitore As Int32) As Boolean
        Dim Document As IDocument = CEUtil.FetchDocByID(os, id)
        Dim IBMAccessPermissionList As IAccessPermissionList = Document.Permissions
        Dim IBMAccessPermission As IAccessPermission
        IBMAccessPermission = Factory.AccessPermission.CreateInstance

        For Each IBMAccessPermission In IBMAccessPermissionList
            Dim str As String = IBMAccessPermission.GranteeName
            If str.Contains(codicefornitore) Then
                Dim IBMAccessPermissionProperties As IProperties
                IBMAccessPermissionProperties = IBMAccessPermission.Properties
                IBMAccessPermissionList.Remove(IBMAccessPermission)
                Exit For
            End If
        Next
        For Each IBMAccessPermission In IBMAccessPermissionList
            Dim str As String = IBMAccessPermission.GranteeName
            If str.Contains("EXTOBJUsers") Then
                Dim IBMAccessPermissionProperties As IProperties
                IBMAccessPermissionProperties = IBMAccessPermission.Properties
                IBMAccessPermissionList.Remove(IBMAccessPermission)
                Exit For
            End If
        Next
        For Each IBMAccessPermission In IBMAccessPermissionList
            Dim str As String = IBMAccessPermission.GranteeName
            If str.Contains("OBJUsers") Then
                Dim IBMAccessPermissionProperties As IProperties
                IBMAccessPermissionProperties = IBMAccessPermission.Properties
                IBMAccessPermissionList.Remove(IBMAccessPermission)
                Exit For
            End If
        Next
        Document.Save(RefreshMode.REFRESH)
    End Function


    Public Shared Function SetFolderPermission(pIBMFolder As IFolder, pUser As String, os As IObjectStore, permesso As String) As Boolean

        Dim Sql = "select * from [Medacta CSP$IBM Folder Security Setup] where UserGroup='" & permesso & "'"
        Dim dt As DataSet
        getConfig(sessiondev, idconn)
        OpenRecordset(Sql, "Table1", dt)
        Dim AccessMask As Int32 = dt.Tables("Table1").Rows(0).Item("Access Mask")
        Dim InheritableDepth As Int32 = dt.Tables("Table1").Rows(0).Item("Inheritable Depth")
        Dim GranteeType As String = dt.Tables("Table1").Rows(0).Item("Grantee Type")






        Dim IBMAccessPermission As IAccessPermission
        Dim IBMAccessPermissionList As IAccessPermissionList
        Dim IBMAccessPermissionProperties As IProperties


        IBMAccessPermission = Factory.AccessPermission.CreateInstance
        IBMAccessPermissionList = pIBMFolder.Permissions

        ' Remove access permission for the OBJUsers group
        For Each IBMAccessPermission In IBMAccessPermissionList
            If IBMAccessPermission.GranteeName.Contains("CN=OBJUsers") Then
                IBMAccessPermissionProperties = IBMAccessPermission.Properties
                IBMAccessPermissionList.Remove(IBMAccessPermission)
                Exit For
            End If
        Next

        ' Remove access permission for the G_SUBS_USERS group
        For Each IBMAccessPermission In IBMAccessPermissionList
            If IBMAccessPermission.GranteeName.Contains("CN=G_SUBS_USERS") Then
                IBMAccessPermissionList.Remove(IBMAccessPermission)
                Exit For
            End If
        Next

        ' Remove access permission for the EXTOBJUsers group
        For Each IBMAccessPermission In IBMAccessPermissionList
            If IBMAccessPermission.GranteeName.Contains("CN=EXTOBJUsers") Then
                IBMAccessPermissionList.Remove(IBMAccessPermission)
                Exit For
            End If
        Next

        ' Add the access permission for the user
        IBMAccessPermission = Factory.AccessPermission.CreateInstance
        IBMAccessPermissionProperties = IBMAccessPermission.Properties
        IBMAccessPermissionProperties.Item("GranteeName") = pUser
        IBMAccessPermissionProperties.Item("GranteeType") = GranteeType ' Assuming "GranteeType" is a property of pRcIBMFolderSetup
        IBMAccessPermissionProperties.Item("AccessMask") = AccessMask ' Assuming "AccessMask" is a property of pRcIBMFolderSetup
        IBMAccessPermissionProperties.Item("InheritableDepth") = InheritableDepth ' Assuming "InheritableDepth" is a property of pRcIBMFolderSetup
        IBMAccessPermissionProperties.Item("PermissionSource") = 1
        IBMAccessPermissionList.Add(IBMAccessPermission)

        pIBMFolder.Save(RefreshMode.REFRESH)
        Return True
    End Function

    Public Shared Function ChangeProdOrderComponent(PhaseString As String, tipo As Int32)
        Try

            Dim sp = Split(PhaseString, "/")
            Dim dt As New DataSet
            getConfig(sessiondev, idconn)
            Dim sql As String
            sql = "SELECT * " & vbCrLf &
                  "FROM [dbo].[ChangeProdOrderComponent]('" & sp(0) & "'," & sp(1) & "," & tipo & ")"
            OpenRecordset(sql, "Table1", dt)
            For Each r As DataRow In dt.Tables("Table1").Rows
                If IsDBNull(r("NewItm")) Then
                    Exit For
                End If
                Using client As WSManufact.WSManufact = New WSManufact.WSManufact

                    Dim myCred = New NetworkCredential("MEDACTA_CH\TesarLinkedSrv", "b6JbuLTeZES1H$3GkyvP")
                    client.UseDefaultCredentials = False
                    client.Credentials = myCred
                    Dim response As Int32 = client.ChangeProdOrderComponent(3, sp(0), sp(1), r("LineComp"), r("NewItm"), r("QtyPer"))
                    client.Dispose()
                    Return response
                End Using

            Next
        Catch ex As Exception
            Return 0
        End Try


    End Function

    ''' <summary>
    ''' Execute the SQL statement passed and return the number of row affected.
    ''' </summary>
    Public Shared Function Execute(ByVal strSQL As String) As Integer
        Dim connection As SqlConnection = Nothing
        Dim command As SqlCommand
        Dim Adapter As New SqlDataAdapter

        Dim rd As Integer
        Try
            connection = New SqlConnection(connectionString)
            connection.Open()

            command = New SqlCommand(strSQL, connection)
            command.CommandTimeout = 60000
            Adapter.SelectCommand = command
            rd = command.ExecuteNonQuery

            connection.Close()
            Return rd
        Catch ex As Exception
            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
            Return 0
        Finally
            If Not connection Is Nothing Then
                connection.Close()
            End If
        End Try
    End Function
    Public Shared Function readrecord(ByVal strSQL As String, pos As Int32) As String

        Dim connection As SqlConnection = Nothing
        Dim command As SqlCommand
        Dim Adapter As New SqlDataAdapter


        Try
            connection = New SqlConnection(connectionString)
            connection.Open()

            command = New SqlCommand(strSQL, connection)
            Adapter.SelectCommand = command
            Dim rd = command.ExecuteReader
            Dim str As String = ""
            While rd.Read()
                str = rd.GetValue(pos)
            End While
            rd.Close()
            connection.Close()
            Return str


        Catch ex As Exception
            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
            Return ""
        Finally
            If Not connection Is Nothing Then
                connection.Close()
            End If
        End Try
    End Function
    Public Shared Function FRW_firma_pdf_tesar_(ReportCB As String, Utente1 As String, tipo As Int32, value As String, btn As Int32, tipofirma As Int32, Utente2 As String)
        'ReportCB = "RouteCardElettronicaOpmultiple"
        'carica_opertaori()
        'Password.ShowDialog()
        'If Autenticato = False Then
        '    MsgBox("Operazione annullata", MsgBoxStyle.Exclamation)
        '    Exit Function
        'End If
        'FrmListaFirmeUtente.user = auttenticuser

        Select Case tipo
            Case 1
                'Dim ck As Int32 = getmacchina(0)
                'Select Case ck
                '    Case 0
                '        Exit Function
                'End Select

                getConfig(sessiondev, idconn)
                Dim sqlupdate As String = "EXECUTE  [dbo].[TesarUnificaIdRilascioStampaControlloEtichetta]  '" & Utente1 & "',''"
                Execute(sqlupdate)
            Case 3


                getConfig(sessiondev, idconn)
                Dim sqlupdate As String = "EXECUTE  [dbo].[TesarUnificaIdRilascioStampaControlloEtichetta]  '" & Utente1 & "',''"
                Execute(sqlupdate)
        End Select
        If btn = 3 Then
            getConfig(sessiondev, idconn)
            Dim sqlupdate As String = "EXECUTE  [dbo].[TesarUnificaIdRilascioStampaControlloEtichetta]  '" & Utente1 & "','BIO'"
            Execute(sqlupdate)
        End If

        'FrmListaFirmeUtente.ShowDialog()
        'If FrmListaFirmeUtente.continua = False Then
        '    Exit Function
        'End If

        'Dim sp = Split(FrmListaFirmeUtente.lotto, "|")

        Dim connection As SqlClient.SqlConnection
        Dim command_Master_read As SqlClient.SqlCommand
        connection = New SqlClient.SqlConnection(connectionString)
        connection.Open()
        Dim strSql As String
        strSql = " SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED SELECT dbo._PRODUZIONE_REPORT_Firma_CameraBianca.[Phase String], " & vbCrLf &
                    "       dbo._PRODUZIONE_REPORT_Firma_CameraBianca.UserFirma2, " & vbCrLf &
                    "       dbo.[Medacta CSP$Prod_ Order Rtng Line Export].Description, " & vbCrLf &
                    "       dbo.[Medacta CSP$Prod_ Order Rtng Line Export].[Lot No_]" & vbCrLf &
                    "FROM dbo._PRODUZIONE_REPORT_Firma_CameraBianca WITH(NOLOCK) " & vbCrLf &
                    "     INNER JOIN dbo.[Medacta CSP$Prod_ Order Rtng Line Export] WITH(NOLOCK) ON dbo._PRODUZIONE_REPORT_Firma_CameraBianca.[Phase String] = dbo.[Medacta CSP$Prod_ Order Rtng Line Export].[Phase String] COLLATE SQL_Latin1_General_CP1_CI_AS" & vbCrLf &
                    "WHERE(dbo._PRODUZIONE_REPORT_Firma_CameraBianca.IDRilascio = '" & value & "')" & vbCrLf &
                    "     AND (dbo._PRODUZIONE_REPORT_Firma_CameraBianca.Firmato = 0)" & vbCrLf &
                    "     AND (dbo._PRODUZIONE_REPORT_Firma_CameraBianca.UserInsert = '" & Utente1 & "' and dbo._PRODUZIONE_REPORT_Firma_CameraBianca.tesar=1)"

        Dim sql_read = "SELECT  [Phase String],[UserFirma2] FROM [dbo].[_PRODUZIONE_REPORT_Firma_CameraBianca]  where [IDRilascio]='" & value & "' and Firmato=0 and [UserInsert]='" & Utente1 & "'"
        command_Master_read = New SqlClient.SqlCommand(strSql, connection)
        Dim r As SqlClient.SqlDataReader = command_Master_read.ExecuteReader()
        Dim IDDocumento As String = ""
        Dim UserOspite As String
        Dim list As New List(Of String)
        list.Clear()
        Dim cnt As Int32 = 0
        While (r.Read())
            list.Add(r.GetValue(0) & "|" & r.GetValue(2) & "|" & r.GetValue(3))
            If IsDBNull(r.GetValue(1)) = False Then
                UserOspite = r.GetValue(1)
            End If
            Try
                If r.GetValue(1) <> "" Then
                    UserOspite = r.GetValue(1)
                End If
            Catch ex As Exception

            End Try
        End While
        r.Close()
        connection.Close()
        connection.Dispose()

        'If firma_auto = 1 Then
        getConfig(sessiondev, idconn)

        Dim Sql = "SELECT UserInsert + '|' + ISNULL(UserFirma2,'') " & vbCrLf &
                    "FROM _PRODUZIONE_REPORT_Firma_CameraBianca " & vbCrLf &
                    "WHERE IDRilascio = '" & value & "'"
        Dim ck = readrecord(Sql, 0)
        Dim spck = Split(ck, "|")

        For i = 0 To 1

            Sql = "SELECT ISNULL(COUNT(fo.id), 0) " & vbCrLf &
                "FROM Operatori op " & vbCrLf &
                "     INNER JOIN _PRODUZIONE_Firme_Operatori fo ON op.id = fo.ID " & vbCrLf &
                "WHERE op.cognomi = '" & Trim(spck(i)) & "';"
            Dim ckcount As Int32 = readrecord(Sql, 0)
            If ckcount = 0 Then
                If Trim(spck(i)) <> "" Then
                    Dim sqlIns As String
                    sqlIns = "INSERT INTO _PRODUZIONE_Firme_Operatori" & vbCrLf &
                        "(id,  " & vbCrLf &
                        " Signature,  " & vbCrLf &
                        " Data " & vbCrLf &
                        ") " & vbCrLf &
                        "       SELECT TOP (1) bk.id,  " & vbCrLf &
                        "                      bk.Signature,  " & vbCrLf &
                        "                      bk.Data " & vbCrLf &
                        "       FROM _PRODUZIONE_Firme_Operatori_Backup bk " & vbCrLf &
                        "            INNER JOIN Operatori op ON bk.ID = op.id " & vbCrLf &
                        "       WHERE op.cognomi = '" & Trim(spck(i)) & "';"
                    Execute(sqlIns)
                End If

            End If
        Next

        'End If


        If list.Count = 0 Then
            MsgBox("Operazione Annullata, lotto non trovato !!", MsgBoxStyle.Exclamation)
            Exit Function
        End If
        'Dim dirallegato As String = ""
        'If allegascontrino = 1 Then
        '    Try
        '        'If sp(1) = -1 Then
        '        Try
        '            Clipboard.Clear()
        '        Catch ex As Exception

        '        End Try
        '        Dim dfstr As String = ""
        '        If My.Settings.defaultdirscontrinoUncPath = "" Then
        '            dfstr = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
        '        Else
        '            dfstr = My.Settings.defaultdirscontrinoUncPath
        '        End If
        '        dirallegato = dfstr & "\" &  id & ".pdf"


        '        FrmScan.auto = True
        '        FrmScan.nome_file = dirallegato
        '        FrmScan.Text = "Creo scansione file:" & dirallegato
        '        FrmScan.ShowDialog()

        '        If My.Computer.FileSystem.FileExists(dirallegato) = False Then
        '            MsgBox("Operazione Annullata, non posso continuare senza la scansione !", MsgBoxStyle.Exclamation)
        '            Exit Function
        '        End If
        '        '
        '        'End If
        '    Catch ex As Exception

        '    End Try
        'End If
        If Not (My.Computer.FileSystem.DirectoryExists("C:\Users\Public\Temp\")) Then
            My.Computer.FileSystem.CreateDirectory("C:\Users\Public\Temp\")
        End If
        If My.Computer.FileSystem.FileExists("C:\Users\Public\Temp\" & value & ".pdf") Then
            My.Computer.FileSystem.DeleteFile("C:\Users\Public\Temp\" & value & ".pdf")
        End If



        Dim ReportViewer1 As New ReportViewer
        Dim serverReport As ServerReport
        serverReport = ReportViewer1.ServerReport
        ReportViewer1.ServerReport.ReportServerUrl = New System.Uri("https://srv-reporting.medacta.locale:4443/reportserver", System.UriKind.Absolute)

        If sessiondev Then
            Select Case idconn
                Case 2
                    'serverReport.ReportServerUrl = New Uri("http://srv-reporterp/ReportServer")
                    'serverReport.ReportPath = "/Companies/[MINT] Medacta INTERNATIONAL/[MAN-PAC] Manufacturing - Packaging/Report_Omogeneita_Lotto"
                Case 1
                    serverReport.ReportPath = "/Reporting Produzione/Interfacce Produzione/Report Dev/" & ReportCB

            End Select
        Else
            serverReport.ReportPath = "/Reporting Produzione/Interfacce Produzione/" & ReportCB
        End If

        ReportViewer1.LocalReport.EnableExternalImages = True
        ReportViewer1.SetDisplayMode(Microsoft.Reporting.WinForms.DisplayMode.PrintLayout)

        'Dim command_Master As New SqlClient.SqlCommand
        'Dim Adapter_Master As New SqlClient.SqlDataAdapter
        'Dim connection = New SqlClient.SqlConnection(Form1.connectionstring)
        'connection.Open()

        Dim ut
        Select Case tipofirma
            Case = 0
                ut = "SELECT *  FROM [dbo].[_PRODUZIONE_REPORT_Firma_CameraBianca] WITH(NOLOCK) where [Lot No_]='" & value & "' and Firmato=0 and [UserInsert]='" & Utente1 & "'"
            Case = 3
                ut = "SELECT *  FROM [dbo].[_PRODUZIONE_REPORT_Firma_CameraBianca] WITH(NOLOCK) where [IDRilascio]='" & value & "'"
            Case Else
                ut = "SELECT *  FROM [dbo].[_PRODUZIONE_REPORT_Firma_CameraBianca] WITH(NOLOCK) where [IDRilascio]='" & value & "' and Firmato=0 and [UserInsert]='" & Utente1 & "'"
        End Select

        Dim command_Master As New SqlClient.SqlCommand
        Dim Adapter_Master As New SqlClient.SqlDataAdapter
        connection.Open()

        command_Master = New SqlClient.SqlCommand(ut, connection)
        Adapter_Master.SelectCommand = command_Master
        Dim rd_ut = command_Master.ExecuteReader
        Dim idr As String = ""
        Dim pashe(10) As String
        Dim a = 0
        While rd_ut.Read
            If tipofirma = 0 Then
                pashe(a) = rd_ut.GetValue(0)
                a = a + 1
            Else
                pashe(0) = rd_ut.GetValue(8)
                a = a + 1
            End If
        End While
        rd_ut.Close()
        If pashe(0) = "" Then
            MsgBox("Lotto non trovato", MsgBoxStyle.Exclamation)
            connection.Close()
            Return 1
        End If
        Array.Resize(pashe, a)

        If tipofirma = 0 Then
            Dim id As New ReportParameter("iddoc", pashe)
            Dim op As New ReportParameter("op2", Utente2)
            ReportViewer1.ServerReport.SetParameters(id)
            ReportViewer1.ServerReport.SetParameters(op)
        Else
            Dim auto As New ReportParameter("autosign", 1)
            Dim id As New ReportParameter("iddoc", pashe)
            Dim op As New ReportParameter("op2", Utente2)
            ReportViewer1.ServerReport.SetParameters(id)
            ReportViewer1.ServerReport.SetParameters(op)
            ReportViewer1.ServerReport.SetParameters(auto)
        End If
        connection.Close()
        Dim bytes As Byte()
        Dim warnings As Warning() = Nothing
        Dim streamids As String() = Nothing
        Dim mimeType As String = Nothing
        Dim encoding As String = Nothing
        Dim extension As String = Nothing
        bytes = ReportViewer1.ServerReport.Render("PDF", Nothing, mimeType, encoding, extension, streamids, warnings)

        Dim nmfile As String = "C:\Users\Public\Temp\" & value & ".pdf"
        File.WriteAllBytes(nmfile, bytes)



        'Dim fs As New FileStream("C:\Users\Public\Temp\" & lotto & ".pdf", FileMode.Create)
        'fs.Write(bytes, 0, bytes.Length)
        'fs.Close()
        'fs.Dispose()
        'If UserOspite <> "" Then
        '    Dim ms = MsgBox("Attenzione, verrà aggiunta la firma dell'utente " & UserOspite & " alla Route Card", MsgBoxStyle.Information + MsgBoxStyle.OkCancel)
        '    If ms = MsgBoxResult.Cancel Then
        '        Exit Function
        '    End If
        'End If

        'FrmReportCa.ShowDialog()
        'If FrmReportCa.continuo = False Then
        '    Exit Function
        'End If
        'Dim opflnm As String
        'opflnm = "C:\Users\Public\Temp\" & id & ".pdf"

        'End If
    End Function
    ''' <summary>
    ''' Execute a massive insert into the table defined of the data contained in the datatable
    ''' </summary>
    Public Shared Function BulkInsert(ByVal strTableName As String, ByVal dt As DataTable) As Integer
        Dim connection As SqlConnection = Nothing
        Dim Adapter As New SqlDataAdapter

        Dim rd As Integer
        Try
            connection = New SqlConnection(connectionString)
            connection.Open()

            Using bulkCopy As New SqlBulkCopy(connection)
                bulkCopy.DestinationTableName = strTableName

                For Each col As DataColumn In dt.Columns
                    If Not col.ColumnName.Contains("xx_") Then
                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName)
                    End If
                Next
                bulkCopy.WriteToServer(dt)
            End Using

            connection.Close()
            Return rd
        Catch ex As Exception
            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
            Return 0
        Finally
            If Not connection Is Nothing Then
                connection.Close()
            End If
        End Try

    End Function

    ''' <summary>
    ''' Execute the update method of the SQLDataAdaper saved with the tablename.
    ''' </summary>
    Public Shared Function UpdateDA(ByRef ds As DataSet, ByVal strTableName As String) As Boolean
        Try
            If DataAdapterDictionary.ContainsKey(strTableName) Then
                Dim command As New SqlCommandBuilder(DataAdapterDictionary.Item(strTableName))
                command.GetDeleteCommand(True)
                command.GetInsertCommand(True)
                command.GetUpdateCommand(True)

                For Each par As SqlParameter In command.GetDeleteCommand(True).Parameters
                    If par.ParameterName.Contains("xx_") Then
                        command.GetDeleteCommand(True).Parameters.Remove(par)
                    End If
                Next
                For Each par As SqlParameter In command.GetInsertCommand(True).Parameters
                    If par.ParameterName.Contains("xx_") Then
                        command.GetInsertCommand(True).Parameters.Remove(par)
                    End If
                Next
                For Each par As SqlParameter In command.GetUpdateCommand(True).Parameters
                    If par.ParameterName.Contains("xx_") Then
                        command.GetUpdateCommand(True).Parameters.Remove(par)
                    End If
                Next

                DataAdapterDictionary.Item(strTableName).Update(ds, strTableName)
                ds.Tables(strTableName).AcceptChanges()
            Else
                Return False
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
            Return False
        End Try

    End Function

    Public Shared Function UpdateDA(ByRef dt As DataTable, ByVal strTableName As String) As Boolean
        Dim ds As New DataSet

        Try
            dt.TableName = strTableName
            ds.Tables.Add(dt.Copy)

            Return UpdateDA(ds, strTableName)

        Catch ex As Exception
            Throw New System.Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description)
            Return False
        End Try

    End Function


    Public Declare Function WNetGetConnection Lib "mpr.dll" Alias _
                   "WNetGetConnectionA" (ByVal lpszLocalName As String,
                   ByVal lpszRemoteName As String, ByRef cbRemoteName As Integer) As Integer

    Public Shared Function GetUNCPathFRK(ByVal sFilePath As String) As String
        Dim allDrives() As DriveInfo = DriveInfo.GetDrives()
        Dim d As DriveInfo
        Dim DriveType, Ctr As Integer
        Dim DriveLtr, UNCName As String
        Dim StrBldr As New StringBuilder

        If sFilePath.StartsWith("\\") Then Return sFilePath

        UNCName = Space(160)
        GetUNCPathFRK = ""

        DriveLtr = sFilePath.Substring(0, 3)

        For Each d In allDrives
            If d.Name = DriveLtr Then
                DriveType = d.DriveType
                Exit For
            End If
        Next

        If DriveType = 4 Then

            Ctr = WNetGetConnection(sFilePath.Substring(0, 2), UNCName, UNCName.Length)

            If Ctr = 0 Then
                UNCName = UNCName.Trim
                For Ctr = 0 To UNCName.Length - 1
                    Dim SingleChar As Char = UNCName(Ctr)
                    Dim asciiValue As Integer = Asc(SingleChar)
                    If asciiValue > 0 Then
                        StrBldr.Append(SingleChar)
                    Else
                        Exit For
                    End If
                Next
                StrBldr.Append(sFilePath.Substring(2))
                GetUNCPathFRK = StrBldr.ToString
            Else
                MsgBox("Cannot Retrieve UNC path", MsgBoxStyle.Critical)
            End If
        Else
            MsgBox("Cannot Use Local Drive", MsgBoxStyle.Critical)
        End If
    End Function

    Private Shared ce As CEConnection
    Public Shared Function archivia_file_ibm(file As String, mime As String, title As String, className As String, fileText As String, ListaProprieta As List(Of String), Devmode As Int32, usabalnce As Int32) As String

        Try

            'ce = New CEConnection
            'Dim sp = Split(connectionibm(Devmode, usabalnce, False), "|")
            'Try
            '    ce.EstablishCredentials(sp(1), sp(2), sp(0))
            'Catch ex As Exception
            '    If usabalnce = 0 Then
            '        Try
            '            sp = Split(connectionibm(Devmode, usabalnce, True), "|")
            '        Catch ex1 As Exception
            '            Return "Errore:" & vbCrLf & ex1.Message
            '        End Try

            '    Else
            '        Return "Errore:" & vbCrLf & ex.Message
            '    End If

            'End Try

            Dim ce As New CEConnection
            Dim sp As String()

            Try
                ' Prima tentativo di connessione
                sp = Split(connectionibm(Devmode, usabalnce, False), "|")
                ce.EstablishCredentials(sp(1), sp(2), sp(0))
            Catch ex As Exception
                ' Se il primo tentativo fallisce
                If usabalnce = 0 Then
                    Try
                        ' Tentativo alternativo
                        sp = Split(connectionibm(Devmode, usabalnce, True), "|")
                        ce.EstablishCredentials(sp(1), sp(2), sp(0))
                    Catch ex1 As Exception
                        ' Ritorno dell'errore dal secondo tentativo
                        Return "Errore durante il tentativo alternativo:" & vbCrLf & ex1.Message
                    End Try
                Else
                    ' Ritorno dell'errore dal primo tentativo
                    Return "Errore durante il primo tentativo:" & vbCrLf & ex.Message
                End If
            End Try


            Dim osNames As ArrayList = ce.GetOSNames()

            Dim doc As IDocument = Nothing
            Dim os As IObjectStore = ce.FetchOS(sp(3))
            doc = CEUtil.CreateDocument(True, file, mime, title, os, className, ListaProprieta)
            doc.Save(RefreshMode.REFRESH)
            Dim folder As String = "/Batch Record Prod"
            If folder.Length = 0 Then folder = "/"
            Dim rcr As IReferentialContainmentRelationship = CEUtil.FileContainable(os, doc, folder)
            rcr.Save(RefreshMode.REFRESH)

            CEUtil.checkInDoc(doc)
            Return doc.Id.ToString

        Catch ex As Exception
            ' Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
            Return "Errore:" & vbCrLf & Err.Description
        End Try


    End Function


    Public Shared Function archivia_file_fornitore(file As String, mime As String, title As String, className As String, Devmode As Int32, usabalnce As Int32, idfolder As String, ListaProprieta As List(Of String)) As String

        Try
            ce = New CEConnection
            Dim sp = Split(connectionibm(Devmode, usabalnce, False), "|")
            ce.EstablishCredentials(sp(1), sp(2), sp(0))

            Dim osNames As ArrayList = ce.GetOSNames()

            Dim doc As IDocument = Nothing
            Dim os As IObjectStore = ce.FetchOS(sp(3))
            doc = CEUtil.CreateDocument(True, file, mime, title, os, className, ListaProprieta)
            doc.Save(RefreshMode.REFRESH)
            Dim folder As IFolder
            'folder = Factory.Folder.FetchInstance(os, idfolder, Nothing)

            folder = Factory.Folder.FetchInstance(os, idfolder, Nothing)

            If folder IsNot Nothing Then
                Dim rcr As IReferentialContainmentRelationship = CEUtil.FileContainable(os, doc, folder.PathName)
                rcr.Save(RefreshMode.REFRESH)

                CEUtil.checkInDoc(doc)
                Return doc.Id.ToString
            Else

                Return "Errore: Impossibile creare o recuperare la cartella."
            End If


            'Dim rcr As IReferentialContainmentRelationship = CEUtil.FileContainable(os, doc, folder.PathName)
            'rcr.Save(RefreshMode.REFRESH)

            'CEUtil.checkInDoc(doc)
            'Return doc.Id.ToString

        Catch ex As Exception
            ' Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
            Return "Errore:" & vbCrLf & Err.Description
        End Try


    End Function

    Public Shared Function elimina_file_ibm(id As String, dev As Int32, usabalance As Int32)
        Try
            ce = New CEConnection
            Dim sp = Split(connectionibm(dev, usabalance, False), "|")
            ce.EstablishCredentials(sp(1), sp(2), sp(0))

            Dim doc As IDocument = Nothing
            Dim os As IObjectStore = ce.FetchOS(sp(3))
            doc = CEUtil.FetchDocByID(os, id)
            If Not doc.Properties.GetProperty("QAApproved").GetStringValue = "Approved" Then


                Dim rcrs As IReferentialContainmentRelationshipSet = doc.Containers
                For Each rcrd As IReferentialContainmentRelationship In rcrs
                    rcrd.Delete()
                    rcrd.Save(RefreshMode.REFRESH)
                    Exit For
                Next
                Dim trashfolder As IFolder = Factory.Folder.FetchInstance(os, "/Trash", Nothing)
                Dim rcrf As IReferentialContainmentRelationship = Nothing
                rcrf = trashfolder.File(doc, AutoUniqueName.AUTO_UNIQUE, doc.Name, DefineSecurityParentage.DO_NOT_DEFINE_SECURITY_PARENTAGE)
                rcrf.Save(RefreshMode.REFRESH)

                Try
                    doc.Properties("LotNo") = CEUtil.createlist("")
                    doc.Save(RefreshMode.REFRESH)
                Catch ex As Exception

                End Try

                Try
                    doc.Properties("ItemNo") = CEUtil.createlist("")
                    doc.Save(RefreshMode.REFRESH)
                Catch ex As Exception

                End Try

                Try
                    doc.Properties("IdRelease") = 0
                    doc.Save(RefreshMode.REFRESH)
                Catch ex As Exception

                End Try


                Try
                    doc.Properties("ProdOrderNo") = CEUtil.createlist("")
                    doc.Save(RefreshMode.REFRESH)
                Catch ex As Exception

                End Try

                Try
                    doc.Properties("PurchaseOrder") = CEUtil.createlist("")
                    doc.Save(RefreshMode.REFRESH)
                Catch ex As Exception

                End Try



                'doc.Delete()
                'doc.Save(RefreshMode.REFRESH)
                Return 1
            Else
                Return 0
            End If

        Catch ex As Exception
            Return 0
        End Try


    End Function
    Public Shared Function get_ibm_link(id As String, dev As Int32, usabalnce As Int32)
        Dim UrlVsid As String
        ce = New CEConnection
        Dim sp = Split(connectionibm(dev, usabalnce, False), "|")
        ce.EstablishCredentials(sp(1), sp(2), sp(0))
        Dim osNames As ArrayList = ce.GetOSNames()
        Dim os As IObjectStore = ce.FetchOS(sp(3))
        Dim doc As FileNet.Api.Core.IDocument = FileNet.Api.Core.Factory.Document.FetchInstance(os, id.Replace("{", "").Replace("}", ""), Nothing)

        Dim vsId() = doc.VersionSeries.CurrentVersion.VersionSeries.ToString.Split("&objectId=")
        Dim StrVsid As String = vsId(1).ToString.Replace("objectId=", "").Replace("{", "").Replace("}", "")
        Dim struri As String = UrlVsid & "bookmark.jsp?desktop=baw&repositoryId=icmcmtos&vsId=%7B#vsid#%7D"
        'Dim StrVsid As String = "907EEF7C-0000-C919-AB09-4D808DDAD8E9"
        Dim str = struri.Replace("#vsid#", StrVsid).ToString
        Return str

    End Function

    Public Shared Function connectionibm(ibmdev As Int32, balance As Int32, errore As Boolean) As String
        getConfig(sessiondev, idconn)
        Dim sql As String
        If ibmdev = 1 Then
            sql = "Select TOP (1) [UriDev], " & vbCrLf &
                   "               [username], " & vbCrLf &
                   "               [password]," & vbCrLf &
                   "                ObjStore" & vbCrLf &
                   "FROM  [dbo].[_IBM_Setup] WITH(NOLOCK) where Tipo=1"
        Else
            If errore Then
                sql = "select  [UriBackUp] + '|' + [UserAdmin] + '|' + [PasswordAdmin] + '|' + ObjStore       FROM  [dbo].[_IBM_Setup] WITH(NOLOCK) where Tipo=1"
            Else
                If balance = 1 Then
                    sql = "select  [UriBalance] + '|' + [UserAdmin] + '|' + [PasswordAdmin] + '|' + ObjStore       FROM  [dbo].[_IBM_Setup] WITH(NOLOCK) where Tipo=1"
                Else
                    sql = "select  [Uri] + '|' + [username] + '|' + [password] + '|' + ObjStore       FROM  [dbo].[_IBM_Setup] WITH(NOLOCK) where Tipo=1"
                End If

            End If
        End If
        Return readrecord(sql, 0)
    End Function



    Public Shared Function download_ibm(ByVal id As String, nomefile As String, ibmdev As Int32, usabalnce As Int32) As Boolean
        Try


            Dim ce As CEConnection
            Dim sp = Split(connectionibm(ibmdev, usabalnce, False), "|")
            ce = New CEConnection
            ce.EstablishCredentials(sp(1), sp(2), sp(0))

            Dim osNames As ArrayList = ce.GetOSNames()
            Dim os As IObjectStore = ce.FetchOS(sp(3))
            Dim doc As IDocument = Factory.Document.FetchInstance(os, id, Nothing)
            Dim elements As IContentElementList = doc.ContentElements
            Dim element As IContentTransfer = CType(elements(0), IContentTransfer)
            Dim filename As String = element.RetrievalName

            Using wavout As IO.Stream = element.AccessContentStream()
                'usnig this test you can avoid exceptions on seeking
                If wavout.CanSeek Then
                    wavout.Seek(0, System.IO.SeekOrigin.Begin)
                End If
                Using fs As IO.FileStream = IO.File.Create(nomefile)
                    Dim buf(1024 * 8) As Byte
                    Dim len As Integer
                    Do
                        len = wavout.Read(buf, 0, buf.Length)
                        If len = 0 Then Exit Do
                        fs.Write(buf, 0, len)
                    Loop
                End Using
            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
            Return False
        End Try
    End Function
    Public Shared Function download_ibmStream(ByVal id As String, ibmdev As Int32, usabalnce As Int32) As Stream
        Try

            Dim ce As CEConnection
            Dim sp = Split(connectionibm(ibmdev, usabalnce, False), "|")
            ce = New CEConnection
            ce.EstablishCredentials(sp(1), sp(2), sp(0))


            Dim osNames As ArrayList = ce.GetOSNames()
            Dim os As IObjectStore = ce.FetchOS(sp(3))
            Dim doc As IDocument = Factory.Document.FetchInstance(os, id, Nothing)
            Dim elements As IContentElementList = doc.ContentElements
            Dim element As IContentTransfer = CType(elements(0), IContentTransfer)
            Dim filename As String = element.RetrievalName


            Dim wavout As IO.Stream = element.AccessContentStream()

            ' Restituisci lo stream originale
            Return wavout


            'Dim memoryStream As New MemoryStream()
            'Using wavout As IO.Stream = element.AccessContentStream()
            '    'usnig this test you can avoid exceptions on seeking

            '    If wavout.CanSeek Then
            '        wavout.Seek(0, System.IO.SeekOrigin.Begin)
            '    End If

            '    ' Copia lo stream risultante in un MemoryStream
            '    wavout.CopyTo(memoryStream)

            '    ' Restituisci il MemoryStream
            '    Return memoryStream
            'End Using

        Catch ex As Exception
            Throw New Exception(System.Reflection.MethodInfo.GetCurrentMethod().Name & vbCrLf & Err.Description, ex)
        End Try

    End Function
    Public Shared Function dir_out_batch_CQ(lotto As String, articolo As String, phasestring As String)
        Try
            Dim anno As Int32
            Dim md_manufact As String = ""
            Dim md_gen_posting_group As String = ""
            Dim MDProductcomponent As String = ""
            Dim InventoryPostingGroupt As String = ""
            Dim tipo As Int32
            Dim cod As Int32
            Dim trovato As Boolean
            Dim doc = Split(phasestring, "/")
            Dim conto_lavoro As String = "0"
            If doc.Length = 3 Then
                conto_lavoro = controllacontolavoro(doc(0), doc(1), 1)
                Dim operazione_conto_lavoro As Int32 = CInt(doc(2))
            End If



            Dim Sql As String
            Dim command_Master As New SqlCommand
            Dim Adapter_Master As New SqlDataAdapter
            Dim connection = New SqlConnection(connectionString)
            connection.Open()


            If conto_lavoro <> "0" And conto_lavoro <> "" Then

                Sql = " Select        dbo.[Medacta CSP$Purchase Header].[FTP Order Type], YEAR(dbo.[Medacta CSP$Purchase Header].[Order Date]) As Anno, " &
                        "                         dbo.[Medacta CSP$Medacta Setup].[Batch Record File Path], dbo.[Medacta CSP$Vendor].[FTP Instr_ In Local Dir], " &
                        "                         dbo.[Medacta CSP$Vendor].[FTP Instr_ In Full Remote Dir], dbo.[Medacta CSP$Vendor].Name, dbo.[Medacta CSP$Vendor].[FTP Instr_ OUT Local Dir], " &
                        "                         dbo.[Medacta CSP$Vendor].[FTP Instr_ OUT Full Remote Dir], dbo.[Medacta CSP$Vendor].[FTP Impl_ In Full Remote Dir], " &
                        "                         dbo.[Medacta CSP$Purchase Header].[Buy-from Vendor No_], dbo.[Medacta CSP$Medacta Setup].[FTP Users Base Folder], " &
                        "                         dbo.[Medacta CSP$Purchase Line].No_ As [Item No_], '' AS [Lot No_], dbo.[Medacta CSP$Item].[Criteria 6], dbo.[Medacta CSP$Item].[Gen_ Prod_ Posting Group], " &
                        "                         dbo.[Medacta CSP$Item].[Criteria 5], dbo.[Medacta CSP$Purchase Line].[Prod_ Order Line No_], dbo.[Medacta CSP$Purchase Line].[Prod_ Order No_]" &
                        " FROM            dbo.[Medacta CSP$Purchase Header] WITH(NOLOCK) INNER JOIN" &
                        "                         dbo.[Medacta CSP$Vendor] ON dbo.[Medacta CSP$Purchase Header].[Buy-from Vendor No_] = dbo.[Medacta CSP$Vendor].No_ INNER JOIN" &
                        "                         dbo.[Medacta CSP$Purchase Line] ON dbo.[Medacta CSP$Purchase Header].No_ = dbo.[Medacta CSP$Purchase Line].[Document No_] INNER JOIN" &
                        "                         dbo.[Medacta CSP$Item] WITH(NOLOCK) ON dbo.[Medacta CSP$Purchase Line].No_ = dbo.[Medacta CSP$Item].No_ CROSS JOIN " &
                        "                         dbo.[Medacta CSP$Medacta Setup] " &
                        " WHERE        (dbo.[Medacta CSP$Purchase Header].No_ = '" & conto_lavoro & "') AND (dbo.[Medacta CSP$Purchase Line].[Prod_ Order Line No_] = " & doc(1) & ") AND  " &
                        "                         (dbo.[Medacta CSP$Purchase Line].[Prod_ Order No_] = '" & doc(0) & "') "
            Else
                Sql = " SELECT     dbo.[Medacta CSP$Purchase Header].[FTP Order Type], YEAR(dbo.[Medacta CSP$Purchase Header].[Order Date]) AS Anno, " &
                                           "                      dbo.[Medacta CSP$Medacta Setup].[Batch Record File Path], dbo.[Medacta CSP$Vendor].[FTP Instr_ IN Local Dir], " &
                                           "                      dbo.[Medacta CSP$Vendor].[FTP Instr_ IN Full Remote Dir], dbo.[Medacta CSP$Vendor].Name, " &
                                           "                      dbo.[Medacta CSP$Vendor].[FTP Instr_ OUT Local Dir], dbo.[Medacta CSP$Vendor].[FTP Instr_ OUT Full Remote Dir], " &
                                           "                      dbo.[Medacta CSP$Vendor].[FTP Impl_ IN Full Remote Dir], dbo.[Medacta CSP$Purchase Header].[Buy-from Vendor No_], " &
                                           "                      dbo.[Medacta CSP$Medacta Setup].[FTP Users Base Folder], derivedtbl_1.[Item No_], derivedtbl_1.[Lot No_],dbo.[Medacta CSP$Item].[Criteria 6],dbo.[Medacta CSP$Item].[Gen_ Prod_ Posting Group],dbo.[Medacta CSP$Item].[Criteria 5], " &
                                           "                      dbo.[Medacta CSP$Item].[Inventory Posting Group] " &
                                           " FROM         dbo.[Medacta CSP$Purchase Header] with (NOLOCK) INNER JOIN " &
                                           "                      dbo.[Medacta CSP$Vendor] ON   " &
                                           "                      dbo.[Medacta CSP$Purchase Header].[Buy-from Vendor No_] = dbo.[Medacta CSP$Vendor].No_ INNER JOIN " &
                                           "                          (SELECT     [Source ID], [Item No_], [Lot No_] " &
                                           "                            FROM          dbo.[Medacta CSP$Tracking Specification] with (NOLOCK)" &
                                           "                            GROUP BY [Source ID], [Item No_], [Lot No_] " &
                                           "                            UNION " &
                                           "                            SELECT     [Source ID], [Item No_], [Lot No_] " &
                                           "                            FROM         dbo.[Medacta CSP$Reservation Entry] with (NOLOCK) " &
                                           "                            GROUP BY [Source ID], [Lot No_], [Item No_]) AS derivedtbl_1 ON dbo.[Medacta CSP$Purchase Header].No_ = derivedtbl_1.[Source ID] INNER JOIN dbo.[Medacta CSP$Item] ON derivedtbl_1.[Item No_] = dbo.[Medacta CSP$Item].No_ CROSS JOIN " &
                                           "                      dbo.[Medacta CSP$Medacta Setup] " &
                                           " WHERE     (dbo.[Medacta CSP$Purchase Header].No_ ='" & doc(0) & "') AND (derivedtbl_1.[Item No_] ='" & articolo & "') AND (derivedtbl_1.[Lot No_] ='" & lotto & "') "
            End If

            command_Master = New SqlCommand(Sql, connection)
            Adapter_Master.SelectCommand = command_Master
            Dim r = command_Master.ExecuteReader
            While r.Read
                cod = r.GetValue(9)
                anno = r.GetValue(1)
                tipo = r.GetValue(0)
                If conto_lavoro <> "0" And conto_lavoro <> "" Then
                    md_manufact = "MED_INT"
                Else
                    md_manufact = r.GetValue(13)
                End If

                md_gen_posting_group = r.GetValue(14)
                Try
                    MDProductcomponent = r.GetValue(15)
                Catch ex As Exception
                    MDProductcomponent = ""
                End Try
                Try
                    InventoryPostingGroupt = r.GetValue(16)
                Catch ex As Exception
                    InventoryPostingGroupt = ""
                End Try
                trovato = True
            End While
            r.Close()
            If trovato = False Then
                Sql = " SELECT        [Criteria 5]  FROM            dbo.[Medacta CSP$Item] WITH(NOLOCK) WHERE        (No_ = N'" & articolo & "')"

                command_Master = New SqlCommand(Sql, connection)
                Adapter_Master.SelectCommand = command_Master
                r = command_Master.ExecuteReader
                While r.Read

                    Try
                        If r.GetString(0) = "TRAY" Then
                            MDProductcomponent = r.GetString(0)
                            trovato = True
                        End If

                    Catch ex As Exception
                        MDProductcomponent = ""
                    End Try

                End While
                r.Close()
            End If
            Try
                Dim canno = Mid(lotto, 1, 2)
                If IsNumeric(CInt(canno)) = True Then
                    anno = calcolanno(canno, anno)
                End If
            Catch ex As Exception
            End Try

            connection.Close()

            Dim reftray As Int32 = 0
            Try
                Select Case MDProductcomponent
                    Case Is = "TRIAL CADDY", "TRAY"
                        reftray = 1
                End Select
            Catch ex As Exception
            End Try

            Dim cfld
            If trovato = False Then
                If InStr(phasestring, "RILAV") > 0 Then
                    cfld = dir_out_batch(lotto, articolo)
                    Return cfld
                End If
            End If

            Dim a1 = Split(lotto, "MP")
            Dim b1 = Split(lotto, "MAT")
            Dim c1 = Split(lotto, "PR")

            If a1.Length = 2 Then
                If b1.Length = 2 Then
                    cfld = path_scansioni_MAT & Replace(lotto, "/", "")
                    'creacartellalotticomulativi(path_scansioni_MAT, md_manufact, "MAT", MDProductcomponent, reftray)
                Else
                    If c1.Length = 2 Then
                        cfld = path_scansioni_PR & Replace(lotto, "/", "")
                        ' creacartellalotticomulativi(path_scansioni_PR, md_manufact, "PR", MDProductcomponent, reftray)
                    Else
                        cfld = dir_out_batch(lotto, articolo)

                    End If

                End If
            Else
                If c1.Length = 2 Then
                    cfld = path_scansioni_PR & Replace(lotto, "/", "")
                    ' creacartellalotticomulativi(path_scansioni_PR, md_manufact, "PR", MDProductcomponent, reftray)
                Else
                    If b1.Length = 2 Then
                        cfld = path_scansioni_MAT & Replace(lotto, "/", "")
                        ' creacartellalotticomulativi(path_scansioni_MAT, md_manufact, "MAT", MDProductcomponent, reftray)
                    Else
                        If md_manufact = "MED_INT" Then
                            If reftray = 1 Then
                                cfld = path_scansioni_tray & Replace(lotto, "/", "")
                            Else
                                cfld = dir_out_batch(lotto, articolo)
                            End If
                        Else

                            If reftray = 1 Then
                                'creacartellalotticomulativi(path_scansioni_tray, md_manufact, "Strumentari", MDProductcomponent, reftray)
                                cfld = path_scansioni_tray & Replace(lotto, "/", "")
                            Else
                                If InventoryPostingGroupt = "SL" And md_gen_posting_group = "MP" Then
                                    cfld = dir_out_batch(lotto, articolo)
                                Else
                                    If md_manufact = "" Or md_manufact = "MED_INT" Then
                                        cfld = dir_out_batch(lotto, articolo)
                                    Else
                                        cfld = path_scansioni_lottoce & Replace(lotto, "/", "")
                                    End If

                                End If
                                '  creacartellalotticomulativi(path_scansioni, md_manufact, "Strumentari", MDProductcomponent, reftray)
                            End If

                        End If
                    End If
                End If
            End If

            Return cfld
        Catch ex As Exception
            insertlog(Err.Description)
        End Try

    End Function

    Public Shared Function calcolanno(annoparz As Int32, anno As Int32)
        Dim annap As Int32 = Year(Now) - CInt(annoparz)
        Dim newanno = 2000 + CInt(annoparz)
        If anno <> newanno Then
            Dim annolimit As Int32 = Year(Now) + 2
            If newanno > annolimit Then
                Year(Now)
            Else
                anno = newanno
            End If
        End If
        Return anno
    End Function


    Public Shared Function controllacontolavoro(Ordine As String, riga As String, tipo As Int32)
        Try
            Dim command_Master As New SqlCommand
            Dim Adapter_Master As New SqlDataAdapter
            Dim connection = New SqlConnection(connectionString)
            connection.Open()
            Dim sql_Operatori = "SELECT        [Buy-from Vendor No_],[Document No_],[Prod_ Order Line No_], [Prod_ Order No_], [Document Type] FROM            dbo.[Medacta CSP$Purchase Line] WITH(NOLOCK) WHERE        ([Prod_ Order No_] = '" & Ordine & "') AND ([Prod_ Order Line No_] =" & riga & ") AND ([Document Type] = 1)"
            command_Master = New SqlCommand(sql_Operatori, connection)
            Adapter_Master.SelectCommand = command_Master
            Dim ra = command_Master.ExecuteReader
            While ra.Read()
                If tipo = 0 Then
                    Dim cod As Int32 = ra.GetValue(0)
                    ra.Close()
                    connection.Close()
                    Return cod
                End If
                If tipo = 1 Then
                    Dim cod As String = ra.GetValue(1)
                    ra.Close()
                    connection.Close()
                    Return cod
                End If
            End While
            ra.Close()
            connection.Close()
            Return 0
        Catch ex As Exception
            insertlog(Err.Description)
        End Try

    End Function

    Public Shared Function copia_file_batch_record(Operazione As String, lotto As String, tipo As Int32, referenza As String, phase As String, idrk As Int64, filedacopiare As String)
        Try
            insertlog("Inizio copia" & My.User.Name)
            path_scansioni_tray = setdirbatch(1700)
            path_scansioni_MAT = setdirbatch(1701)
            path_scansioni_PR = setdirbatch(1702)
            path_scansioni_lottoce = setdirbatch(9999)

            Dim dir = "\\srv-production\GestionaleProduzione\BatchRecordProdTemp\" & Replace(lotto, "/", "") & "\" & Operazione
            Dim copiato = 0
            Dim dirout As String = ""
            ' Dim tipo As Int32 = ra.GetValue(13)
            Select Case tipo
                Case = 0
                    dirout = dir_out_batch(Replace(lotto, "/", ""), referenza)
                Case = 1

                    dirout = dir_out_batch_CQ(Replace(lotto, "/", ""), referenza, phase)
                    If dirout = "" Then
                        dirout = dir_out_batch(Replace(lotto, "/", ""), referenza)
                    End If

                Case Is = 4
                    dirout = dir_out_batch_CQ(Replace(lotto, "/", ""), referenza, phase)
                    If My.Computer.FileSystem.FileExists(filedacopiare) Then
                        Dim flname = My.Computer.FileSystem.GetFileInfo(filedacopiare)
                        Dim Fltobecopy As String = ""
                        Dim sovrascrivi As Boolean = False
                        Try
                            If lotto = "NO LOT" Then
                                If Operazione > 10000 Then
                                    Fltobecopy = dirout & "\" & referenza & "\Data_" & Operazione & "\" & flname.Name
                                    sovrascrivi = True
                                Else
                                    Fltobecopy = dirout & "\" & referenza & "\" & Operazione & "\" & flname.Name
                                    sovrascrivi = False
                                End If
                            Else
                                If Operazione > 10000 Then
                                    Fltobecopy = dirout & "\Data_" & Operazione & "\" & flname.Name
                                    sovrascrivi = True
                                Else
                                    Fltobecopy = dirout & "\" & Operazione & "\" & flname.Name
                                    sovrascrivi = False
                                End If
                            End If
                            My.Computer.FileSystem.CopyFile(filedacopiare, Fltobecopy, True)
                            copiato = 1
                            If Operazione > 10000 Then
                                update(idrk, copiato, dirout & "\Data_" & Operazione, tipo, "-")
                            Else
                                update(idrk, copiato, dirout & "\" & Operazione, tipo, "-")
                            End If

                            GoTo salta

                        Catch ex As Exception
                            update(idrk, 1, "Errore Copia: " & Now.ToString("gg/MM/yyyy"), tipo, "-")
                            copiato = 0
                        End Try

                    End If
                Case Is = 5
                    If My.Computer.FileSystem.FileExists(filedacopiare) Then
                        My.Computer.FileSystem.DeleteFile(filedacopiare)
                        copiato = 1
                        update(idrk, copiato, "Eliminato-" & filedacopiare, tipo, "-")
                        dirout = ""
                    End If
            End Select

            If dirout <> "" Then
                If Operazione <> 9999 Then
                    If My.Computer.FileSystem.DirectoryExists(dir) = True Then


                        Dim Dirtobecopy As String = ""
                        Dim sovrascrivi As Boolean = False

                        Try
                            If lotto = "NO LOT" Then
                                If Operazione > 10000 Then
                                    Dirtobecopy = dirout & "\" & referenza & "\Data_" & Operazione
                                    sovrascrivi = True
                                Else
                                    Dirtobecopy = dirout & "\" & referenza & "\" & Operazione
                                    sovrascrivi = False
                                End If
                            Else
                                If Operazione > 10000 Then
                                    Dirtobecopy = dirout & "\Data_" & Operazione
                                    sovrascrivi = True
                                Else
                                    Dirtobecopy = dirout & "\" & Operazione
                                    sovrascrivi = False
                                End If
                            End If

                            Dim cp As Boolean
                            If My.Computer.FileSystem.DirectoryExists(Dirtobecopy) = True Then
                                Dim p = My.Computer.FileSystem.GetDirectoryInfo(dir).GetFiles.Count
                                cp = confrontadir(dir, Dirtobecopy)
                            Else
                                cp = True
                            End If

                            If cp = True Then
                                If My.Computer.FileSystem.DirectoryExists(dir) Then
                                    My.Computer.FileSystem.MoveDirectory(dir, Dirtobecopy, True)
                                    If My.Computer.FileSystem.DirectoryExists(Dirtobecopy) = True Then
                                        copiato = 1
                                    End If
                                    update(idrk, copiato, Dirtobecopy, tipo, "-")
                                Else

                                    update(idrk, 1, Dirtobecopy, tipo, "-")
                                End If

                            Else
                                If My.Computer.FileSystem.DirectoryExists(dir) = True Then
                                    My.Computer.FileSystem.DeleteDirectory(dir, FileIO.DeleteDirectoryOption.DeleteAllContents)
                                    copiato = 1
                                    update(idrk, copiato, Dirtobecopy, tipo, "-")
                                End If
                            End If
                        Catch ex As Exception
                            update(idrk, 1, "Errore Copia: " & Now.ToString("gg/MM/yyyy"), tipo, Mid(ex.Message, 1, 254))
                        End Try
                    Else
                        update(idrk, copiato, "", tipo, "-")
                    End If
                End If

            End If
salta:
            insertlog("Fine copia id:" & idrk)
            Return True
        Catch ex As Exception
            update(idrk, 1, "Errore Copia: " & Now.ToString("gg/MM/yyyy"), tipo, Mid(ex.Message, 1, 254))
            Return False
        End Try
    End Function
    Public Shared Function setdirbatch(anno As Int32)
        Dim connection As New SqlConnection
        Dim Adapter_Master As New SqlDataAdapter
        connection = New SqlConnection(connectionString)
        connection.Open()
        Dim strSql As String
        strSql = "SELECT *" & vbCrLf &
                        "FROM [dbo].[_SMG_Setup_Directory_BatchRecord_Anno] WITH(NOLOCK)" & vbCrLf &
                        "WHERE [CQ] = 1 and [anno]=" & anno & vbCrLf &
                        "ORDER BY [ordine]"
        Dim command_Master = New SqlCommand(strSql, connection)
        Dim r As SqlDataReader = command_Master.ExecuteReader()
        Dim str As String = ""
        While (r.Read())
            str = r.GetValue(1)
        End While
        r.Close()
        connection.Close()
        Return str
    End Function
    Public Shared Function dir_out_batch(lotto As String, articolo As String)
        Try
            Dim connection As New SqlConnection
            Dim Adapter_Master As New SqlDataAdapter
            connection = New SqlConnection(connectionString)
            connection.Open()
            Dim anno '= aa + CInt(Mid(lotto, 1, 2))
            Dim canno = Mid(lotto, 1, 2)
            If IsNumeric(canno) = False Then
                If canno = "HC" Then
                    anno = 1703
                    'GoTo salta
                Else
                    canno = Mid(Now.Year, 1, 2)
                    anno = calcolanno(canno, anno)
                End If
            Else
                anno = calcolanno(canno, anno)
            End If
            'salta:
            If anno = 0 Then
                anno = 9999
            End If
            Dim sql_read = " SELECT     dbo.[Medacta CSP$Item].No_, dbo.[Medacta CSP$Item].[Criteria 2], dbo.[Medacta CSP$Item].[Gen_ Prod_ Posting Group], " &
                            "                      dbo._SMG_Setup_Directory_BatchRecord_Anno.anno, dbo._SMG_Setup_Directory_BatchRecord_Anno.dir,dbo._SMG_Setup_Directory_BatchRecord_Anno.[Inverti Anno],[Routing No_],dbo.[Medacta CSP$Item].[Criteria 6],dbo.[Medacta CSP$Item].[Final Stock Destination] " &
                            " FROM         dbo.[Medacta CSP$Item] WITH (NOLOCK) CROSS JOIN " &
                            "                      dbo._SMG_Setup_Directory_BatchRecord_Anno " &
                            " WHERE     (dbo.[Medacta CSP$Item].No_ = '" & articolo & "') AND (dbo._SMG_Setup_Directory_BatchRecord_Anno.anno =" & anno & ")"

            Dim command_Master = New SqlCommand(sql_read, connection)
            Dim r As SqlDataReader = command_Master.ExecuteReader()
            Dim MDSubItemcategory As String
            Dim MDManufact As String
            Dim Gen_Prod_PostingGroup As String
            Dim routing As String = ""
            Dim inverti_anno As Boolean
            Dim finalstock As String = ""
            While (r.Read())
                MDSubItemcategory = r.GetValue(1)
                Gen_Prod_PostingGroup = r.GetValue(2)
                DirScansioniBatchRecord = r.GetValue(4)
                inverti_anno = r.GetValue(5)
                Try
                    routing = r.GetValue(6)
                Catch ex As Exception

                End Try
                Try
                    MDManufact = r.GetValue(7)
                Catch ex As Exception
                    MDManufact = "MED_INT"
                End Try
                Try
                    finalstock = r.GetValue(8)
                Catch ex As Exception
                    finalstock = ""
                End Try
            End While
            r.Close()
            connection.Close()
            Dim flddest As String = ""
            If inverti_anno = True Then
                If Gen_Prod_PostingGroup = "SPINE-MP" Or Gen_Prod_PostingGroup = "SPINE-PF" Then
                    flddest = DirScansioniBatchRecord & "\" & anno & "\impianti\" & lotto
                Else
                    If MDSubItemcategory = "IMPLANT" Then
                        flddest = DirScansioniBatchRecord & "\" & anno & "\impianti\" & lotto
                    Else
                        flddest = DirScansioniBatchRecord & "\" & anno & "\strumentari\" & lotto
                    End If
                End If
            Else
                Select Case canno
                    Case "HC"
                        flddest = DirScansioniBatchRecord & lotto
                    Case Else
                        If Gen_Prod_PostingGroup = "SPINE-MP" Or Gen_Prod_PostingGroup = "SPINE-PF" Then
                            flddest = DirScansioniBatchRecord & "\impianti\" & anno & "\" & lotto
                        Else
                            If MDSubItemcategory = "IMPLANT" Then
                                flddest = DirScansioniBatchRecord & "\impianti\" & anno & "\" & lotto
                            Else
                                If Mid(routing, 1, 1) = "9" Then
                                    flddest = DirScansioniBatchRecord & "\impianti\" & anno & "\" & lotto
                                Else
                                    If anno = 0 Then
                                        If MDManufact <> "MED_INT" Then
                                            flddest = DirScansioniBatchRecord & "\" & lotto
                                        Else
                                            flddest = DirScansioniBatchRecord & "\strumentari\" & anno & "\" & lotto
                                        End If

                                    Else
                                        Dim tipo As Int32 = getfinalstock(finalstock)
                                        If tipo = 0 Then
                                            flddest = DirScansioniBatchRecord & "\impianti\" & anno & "\" & lotto
                                        Else
                                            flddest = DirScansioniBatchRecord & "\strumentari\" & anno & "\" & lotto
                                        End If

                                    End If

                                End If

                            End If
                        End If

                End Select

            End If

            Return flddest
        Catch ex As Exception
            insertlog(Err.Description)
            'EventLog.WriteEntry("CopyBatchRecord", Err.Description, EventLogEntryType.Information)
            Return ""
        End Try


    End Function
    Public Shared Function getfinalstock(magazzino As String)
        Try
            Dim command_Master As New SqlCommand
            Dim Adapter_Master As New SqlDataAdapter
            Dim connection = New SqlConnection(connectionString)
            connection.Open()
            Dim strSql As String
            strSql = "SELECT [Final Stock Destination], " & vbCrLf &
                        "       [Tipo]" & vbCrLf &
                        "FROM [Db_Nav_Prod].[dbo].[_SMG_Setup_MagazziniCopiaBatchRecord] WITH(NOLOCK)" & vbCrLf &
                        "WHERE [Final Stock Destination] = '" & magazzino & "'"
            command_Master = New SqlCommand(strSql, connection)
            Adapter_Master.SelectCommand = command_Master
            Dim ra = command_Master.ExecuteReader
            Dim tipo As Int32 = 1
            While ra.Read()
                tipo = ra.GetValue(1)
            End While
            ra.Close()
            connection.Close()
            Return tipo
        Catch ex As Exception
            insertlog(Err.Description)
        End Try


    End Function

    Private Shared Function update(idrk As Int32, copiato As Int32, dir As String, tipo As Int32, errore As String)
        Dim connection As New SqlConnection
        Dim command_Master As New SqlCommand
        Dim Adapter_Master As New SqlDataAdapter
        connection = New SqlConnection(connectionString)
        connection.Open()
        Dim sql_updt = " UPDATE [dbo].[_PRODUZIONE_queue_copy_batch_record] " &
                        "   SET [Dir]='" & dir & "',[Stato] = 1 , [Copiato]=" & copiato & ",Errore='" & errore.Replace("'", "") & "'" &
                        "  WHERE IDRK=" & idrk & " and tipo=" & tipo
        command_Master = New SqlCommand(sql_updt, connection)
        Dim ra = command_Master.ExecuteNonQuery()
        connection.Close()

    End Function
    Private Shared Function insertlog(desc As String)
        Dim connection As New SqlConnection
        Dim command_Master As New SqlCommand
        Dim Adapter_Master As New SqlDataAdapter
        connection = New SqlConnection(connectionString)
        connection.Open()
        Dim strSql As String
        strSql = "INSERT INTO [dbo].[_Log_Copia_Batch_Record]" & vbCrLf &
                    "           ([Description]" & vbCrLf &
                    "           ,[date])" & vbCrLf &
                    "     VALUES" & vbCrLf &
                    "           ('" & Replace(Replace(desc, """", ""), "", "") & "'" & vbCrLf &
                    "           ,GETDATE())"

        command_Master = New SqlCommand(strSql, connection)
        Dim ra = command_Master.ExecuteNonQuery()
        connection.Close()

    End Function
    Public Class FileCompare
        Implements System.Collections.Generic.IEqualityComparer(Of System.IO.FileInfo)

        Public Function Equals1(ByVal x As System.IO.FileInfo, ByVal y As System.IO.FileInfo) _
            As Boolean Implements System.Collections.Generic.IEqualityComparer(Of System.IO.FileInfo).Equals

            Return (x.Name = y.Name) And (x.Length = y.Length)
        End Function

        ' Return a hash that reflects the comparison criteria. According to the 
        ' rules for IEqualityComparer(Of T), if Equals is true, then the hash codes must
        ' also be equal. Because equality as defined here is a simple value equality, not
        ' reference identity, it is possible that two or more objects will produce the same
        ' hash code.
        Public Function GetHashCode1(ByVal fi As System.IO.FileInfo) _
            As Integer Implements System.Collections.Generic.IEqualityComparer(Of System.IO.FileInfo).GetHashCode
            Dim s As String = fi.Name & fi.Length
            Return s.GetHashCode()
        End Function
    End Class
    Public Shared Function confrontadir(d1 As String, d2 As String)

        ' Create two identical or different temporary folders
        ' on a local drive and add files to them.
        ' Then set these file paths accordingly.

        Dim pathA As String = d1
        Dim pathB As String = d2
        Dim str As String
        ' Take a snapshot of the file system.
        Dim dir1 As New System.IO.DirectoryInfo(pathA)
        Dim dir2 As New System.IO.DirectoryInfo(pathB)

        Dim list1 = dir1.GetFiles("*.*", System.IO.SearchOption.AllDirectories)
        Dim list2 = dir2.GetFiles("*.*", System.IO.SearchOption.AllDirectories)

        ' Create the FileCompare object we'll use in each query
        Dim myFileCompare As New FileCompare

        ' This query determines whether the two folders contain
        ' identical file lists, based on the custom file comparer
        ' that is defined in the FileCompare class.
        ' The query executes immediately because it returns a bool.
        Dim areIdentical As Boolean = list1.SequenceEqual(list2, myFileCompare)
        If areIdentical = True Then
            Return False
        Else
            Return True
        End If

        ' Find common files in both folders. It produces a sequence and doesn't execute
        ' until the foreach statement.
        Dim queryCommonFiles = list1.Intersect(list2, myFileCompare)

        If queryCommonFiles.Count() > 0 Then

            str = "The following files are in both folders:"
            For Each fi As System.IO.FileInfo In queryCommonFiles
                str = str & vbCrLf & (fi.FullName)
            Next
        Else
            str = "There are no common files in the two folders."
        End If

        ' Find the set difference between the two folders.
        ' For this example we only check one way.
        Dim queryDirAOnly = list1.Except(list2, myFileCompare)
        str = "The following files are in dirA but not dirB:"
        For Each fi As System.IO.FileInfo In queryDirAOnly
            str = str & vbCrLf & (fi.FullName)
        Next

        ' Keep the console window open in debug mode

    End Function

#Disable Warning BC42021 ' Function without an 'As' clause
    Public Shared Function NavigateReport(theUrl As String)
#Enable Warning BC42021 ' Function without an 'As' clause
        Try
            'Dim brows = getDefaultBrowser()
            If My.Computer.FileSystem.FileExists("C:\Program Files (x86)\Mozilla Firefox\Firefox.exe") Then
                Dim p As New Process
                p.StartInfo.WindowStyle = ProcessWindowStyle.Normal
                p.StartInfo.FileName = "firefox"
                p.StartInfo.Arguments = theUrl
                p.Start()
            ElseIf My.Computer.FileSystem.FileExists("C:\Program Files\Google\Chrome\Application\Chrome.exe") And (Not theUrl.Contains("http://192.168.254.14")) Then
                Dim p As New Process
                p.StartInfo.WindowStyle = ProcessWindowStyle.Normal
                p.StartInfo.FileName = "chrome"
                p.StartInfo.Arguments = theUrl
                p.Start()
            Else
                Dim ie As New InternetExplorer
                ie.Navigate2(theUrl)
                ie.Visible = True
            End If
        Catch ex As Exception
            MsgBox("" & Err.Description)
        End Try
    End Function

    Public Shared Sub spedisci_nav(ByVal test As Int32, ByVal soggetto As String, ByVal oggetto As String, ByVal usrsend As String, ByVal filename As String)
        Try
            If sessiondev = True Then
                Exit Sub
            End If
            getConfig(sessiondev, idconn)
            Dim Adapter_Master As New SqlDataAdapter
            Dim command_Master_read As SqlCommand
            Dim connection As New SqlConnection(connectionString)
            connection.Open()
            Dim RecordCount As Integer = 0
            Dim lista_utenti As New Microsoft.VisualBasic.Collection
            lista_utenti.Clear()
            Dim sql_read As String
            If test = 2 Then
                sql_read = "select * from  [_SMG_Setup_Utenti] where [Errori Produzione Strum]=1"
            Else
                sql_read = "select * from  [_SMG_Setup_Utenti] where [Smg Lotti]=1"
            End If
            command_Master_read = New SqlCommand(sql_read, connection)
            Dim r = command_Master_read.ExecuteReader()
            While (r.Read())
                lista_utenti.Add(r.GetValue(1))
            End While
            r.Close()
            Adapter_Master.Dispose()
            connection.Close()
            Dim Recipients As String
            Dim SenderName As String = "Medacta CSP"
            Dim SenderAddress As String = "it@medacta.ch"
            Dim Subject As String
            Dim Body As String
            Dim HtmlFormatted As Int32 = 1
            Dim CC As String

            Select Case test
                Case Is = 1
                    Recipients = "mabbate@medacta.ch"
                    'lista_mail.Clear()
                Case Is = 5
                    Recipients = "badet@medacta.ch;falletta@medacta.ch;SalaDanna@medacta.ch"
                Case Is = 6
                    Recipients = "falletta@medacta.ch"
                Case Is = 7
                    Recipients = "mabbate@medacta.ch;pertusini@medacta.ch;lunanova@medacta.ch"
                Case Is = 8
                    'lista_mail.Clear()
                    Recipients = usrsend
                    CC = "mabbate@medacta.ch"
                Case Is = 10
                    lista_utenti.Clear()
                    connection.Open()
                    sql_read = "select * from  [_SMG_Setup_Utenti] where [Validazioni]=1"
                    command_Master_read = New SqlCommand(sql_read, connection)
                    r = command_Master_read.ExecuteReader()
                    Recipients = ""
                    While (r.Read())
                        lista_utenti.Add(r.GetValue(1))
                    End While
                    r.Close()
                    connection.Close()
                    For ut = 0 To lista_utenti.Count - 1
                        Dim indirizzo = lista_utenti(ut + 1)
                        If ut = 0 Then
                            Recipients = indirizzo
                        Else
                            Recipients = Recipients & ";" & indirizzo
                        End If
                    Next
                    CC = "mabbate@medacta.ch"
                Case Is = 1001
                    lista_utenti.Clear()
                    connection.Open()
                    sql_read = "select * from  [_PRODUZIONE_descrizione_macchine_mail_alert] where [mdc_machine]='" & usrsend & "' order by tipo"
                    command_Master_read = New SqlCommand(sql_read, connection)
                    r = command_Master_read.ExecuteReader()
                    Recipients = ""
                    CC = ""
                    While (r.Read())

                        If r.GetValue(2) = "A" Then
                            If Recipients = "" Then
                                Recipients = r.GetValue(1)
                            Else
                                Recipients = Recipients & ";" & r.GetValue(1)
                            End If
                        Else
                            If CC = "" Then
                                CC = r.GetValue(1)
                            Else
                                CC = CC & ";" & r.GetValue(1)
                            End If
                        End If

                    End While
                    r.Close()
                    connection.Close()
                    If Recipients = "" Then
                        MsgBox("Errore, setup mail non eseguito per macchina " & usrsend, MsgBoxStyle.Exclamation)
                        Exit Sub
                    End If
                Case Is = 1002
                    Recipients = usrsend
                    CC = "mabbate@medacta.ch;duccoli@medacta.ch;fava@medacta.ch"
                Case Else
                    For ut = 0 To lista_utenti.Count - 1
                        Dim indirizzo = lista_utenti(ut + 1)
                        If ut = 0 Then
                            Recipients = indirizzo
                        Else
                            Recipients = Recipients & ";" & indirizzo
                        End If
                    Next
            End Select

            Subject = soggetto
            If oggetto <> "" Then
                Body = oggetto
            End If

            send_mail_buffer(SenderName, SenderAddress, Recipients, Subject, Body, HtmlFormatted, filename, CC, 0)
            'lista_mail.Clear()
        Catch ex As Exception
            MsgBox("" & Err.Description)
        End Try
    End Sub

    Public Shared Function send_mail_buffer(SenderName As String, SenderAddress As String, Recipients As String, Subject As String, Body As String, HtmlFormatted As Int32, FileName As String, CC As String, Sent As Int32)
        Dim connection As SqlConnection
        Dim Adapter_Master As New SqlDataAdapter
        Dim command_Master_read As SqlCommand
        connection = New SqlConnection(connectionString)
        'connection = New SqlConnection("Server=SRV-NAVDEV\NAV2016;Database=Db_Nav_Prod;Uid=Medacta_Client_NavAccess;Pwd=mdc2011;")
        Try
            connection.Open()
            Dim sql_read = " INSERT INTO [dbo].[Mail Buffer] " &
                            "           ([EntryNo] " &
                            "           ,[SenderName] " &
                            "           ,[SenderAddress] " &
                            "           ,[Recipients] " &
                            "           ,[Subject] " &
                            "           ,[Body] " &
                            "           ,[HtmlFormatted] " &
                            "           ,[FileName] " &
                            "           ,[CC] " &
                            "           ,[Sent]) " &
                            "     VALUES " &
                            "           ((SELECT MAX([EntryNo])+1 FROM [dbo].[Mail Buffer])" &
                            "           ,'" & SenderName & "'" &
                            "           ,'" & SenderAddress & "'" &
                            "           ,'" & Recipients & "'" &
                            "           ,'" & Replace(Subject, "'", "´") & "'" &
                            "           ,'" & Body & "'" &
                            "           ," & HtmlFormatted &
                            "           ,'" & FileName & "'" &
                            "           ,'" & CC & "'" &
                            "           ," & Sent & ")"
            command_Master_read = New SqlCommand(sql_read, connection)
            Dim r = command_Master_read.ExecuteNonQuery
            Adapter_Master.Dispose()
            connection.Close()
            Return True
        Catch ex As Exception
            MsgBox("" & Err.Description)
            Return False
        End Try
    End Function

    Public Shared Function SearchAndAddToListWithFilter(ByVal path As String, ByVal filters As String(), ByVal searchSubFolders As Boolean) As List(Of IO.FileInfo)
        If Not IO.Directory.Exists(path) Then
            Throw New Exception("Path not found")
        End If

        Dim searchOptions As IO.SearchOption
        If searchSubFolders Then
            searchOptions = IO.SearchOption.AllDirectories
        Else
            searchOptions = IO.SearchOption.TopDirectoryOnly
        End If

        Try
            Return filters.SelectMany(Function(filter) New IO.DirectoryInfo(path).GetFiles(filter, searchOptions)).ToList
        Catch ex As Exception

        End Try

    End Function

    Public Shared Sub OpenProcess(FileName As String, wstyle As ProcessWindowStyle, FoundMessage As String)
        Dim MyProcess As New Process
        MyProcess.StartInfo.WindowStyle = wstyle
        MyProcess.StartInfo.CreateNoWindow = False
        MyProcess.StartInfo.Verb = "open"
        MyProcess.StartInfo.FileName = FileName
        MyProcess.Start()
        If FoundMessage <> "" Then
            Try
                Call MessageTimeOut(FoundMessage, "", 10)
            Catch ex As Exception
                MsgBox(FoundMessage, MsgBoxStyle.Information)
            End Try
        End If

    End Sub

    Public Shared Function MessageTimeOut(sMessage As String, sTitle As String, iSeconds As Integer) As Boolean
        Dim Shell = CreateObject("WScript.Shell")
        Dim v = Shell.Run("mshta.exe vbscript:close(CreateObject(""WScript.shell"").Popup(""" & sMessage & """," & iSeconds & ",""" & sTitle & """))")
        MessageTimeOut = True
    End Function


    ''' <summary>
    ''' Recupera il singolo db record
    ''' </summary>
    ''' <param name="strSQL"> query </param>
    ''' <returns> dizionario chiave valore </returns>
    Public Shared Function getItem(strSQL As String) As Dictionary(Of String, String)
        Dim connection As SqlConnection = Nothing
        Dim command As SqlCommand
        Dim Adapter As New SqlDataAdapter
        Dim dict As Dictionary(Of String, String) = New Dictionary(Of String, String)()
        Try
            connection = New SqlConnection(connectionString)
            connection.Open()

            command = New SqlCommand(strSQL, connection)
            Adapter.SelectCommand = command
            Dim rd = command.ExecuteReader
            If rd.Read() Then
                For idx As Integer = 0 To rd.FieldCount - 1
                    dict.Add(rd.GetName(idx), rd.GetValue(idx).ToString())
                Next
            End If
            rd.Close()
            connection.Close()

            Return dict

        Catch ex As Exception
            Return dict
        Finally
            If Not connection Is Nothing Then
                connection.Close()
            End If
        End Try
    End Function

    ''' <summary>
    ''' Recupera tutti i records risultatinti dalla query passata come parametro 
    ''' </summary>
    ''' <param name="strSQL"> query </param>
    ''' <returns> lista di dizionari chiave valore </returns>

    Public Shared Function getItemList(strSQL As String) As List(Of Dictionary(Of String, String))
        Dim connection As SqlConnection = Nothing
        Dim command As SqlCommand
        Dim Adapter As New SqlDataAdapter
        Dim dict = New Dictionary(Of String, String)()
        Dim List = New List(Of Dictionary(Of String, String))()
        Try
            connection = New SqlConnection(connectionString)
            connection.Open()

            command = New SqlCommand(strSQL, connection)
            Adapter.SelectCommand = command
            Dim rd = command.ExecuteReader
            While rd.Read()
                dict = New Dictionary(Of String, String)()
                For idx As Integer = 0 To rd.FieldCount - 1
                    dict.Add(rd.GetName(idx), rd.GetValue(idx).ToString())
                Next
                List.Add(dict)
            End While

            rd.Close()
            connection.Close()
            Return List

        Catch ex As Exception
            Return List
        Finally
            If Not connection Is Nothing Then
                connection.Close()
            End If
        End Try
    End Function

End Class
