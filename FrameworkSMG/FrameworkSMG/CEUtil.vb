﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.IO
Imports FileNet.Api.Core
Imports FileNet.Api.Collection
Imports FileNet.Api.Constants
Imports FileNet.Api.Util
Imports System.Collections
Imports FileNet.Api.Security
Imports FileNet.Api.Query
Imports FileNet.Api.[Property]


Public Class CEUtil
    Public Shared Function ReadContentFromFile(ByVal fileName As String) As Byte()
        Dim fi As FileInfo = New FileInfo(fileName)
        Dim numBytes As Long = fi.Length
        Dim buffer As Byte() = Nothing

        If numBytes > 0 Then

            Try
                Dim fs As FileStream = New FileStream(fileName, FileMode.Open, FileAccess.Read)
                Dim br As BinaryReader = New BinaryReader(fs)
                buffer = br.ReadBytes(CInt(numBytes))
                br.Close()
                fs.Close()
            Catch e As Exception
                System.Console.WriteLine(e.StackTrace)
            End Try
        End If

        Return buffer
    End Function

    Public Shared Sub WriteContentToFile(ByVal doc As IDocument, ByVal path As String)
        Dim fileName As String = doc.Name
        Dim file As String = path & fileName

        Try
            Dim fs As FileStream = New FileStream(file, FileMode.CreateNew)
            Dim bw As BinaryWriter = New BinaryWriter(fs)
            Dim s As Stream = doc.AccessContentStream(0)
            Dim data As Byte() = New Byte(s.Length - 1) {}
            s.Read(data, 0, data.Length)
            s.Close()
            bw.Write(data)
            bw.Close()
            fs.Close()
        Catch e As Exception
            System.Console.WriteLine(e.StackTrace)
        End Try
    End Sub

    Public Shared Function CreateContentTransfer(ByVal fileName As String) As IContentTransfer
        Dim ct As IContentTransfer = Nothing
        Dim fi As FileInfo = New FileInfo(fileName)

        If ReadContentFromFile(fileName) IsNot Nothing Then
            ct = Factory.ContentTransfer.CreateInstance()
            Dim s As Stream = New MemoryStream(ReadContentFromFile(fileName))
            ct.SetCaptureSource(s)
            ct.RetrievalName = fi.Name
        End If

        Return ct
    End Function

    Public Shared Function CreateContentElementList(ByVal fileName As String) As IContentElementList
        Dim cel As IContentElementList = Nothing

        If CreateContentTransfer(fileName) IsNot Nothing Then
            cel = Factory.ContentElement.CreateList()
            Dim ct As IContentTransfer = CreateContentTransfer(fileName)
            cel.Add(ct)
        End If

        Return cel
    End Function

    Public Shared Function CreateDocument(ByVal withContent As Boolean, ByVal file As String, ByVal mimeType As String, ByVal docName As String, ByVal os As IObjectStore, ByVal className As String, ListaProprieta As List(Of String)) As IDocument
        Dim doc As IDocument = Nothing

        If className.Equals("") Then
            doc = Factory.Document.CreateInstance(os, Nothing)
        Else
            doc = Factory.Document.CreateInstance(os, className)
        End If


        For Each ls In ListaProprieta

            Dim sp = Split(ls, "|")
            Select Case sp(2)
                Case "m"

                    doc.Properties(sp(0)) = createlist(sp(1))
                Case "s"
                    doc.Properties(sp(0)) = sp(1)
                Case "i"
                    doc.Properties(sp(0)) = Convert.ToInt32(sp(1))
                Case "d"
                    doc.Properties(sp(0)) = CDate(sp(1))
                Case "im"
                    doc.Properties(sp(0)) = createlistint(sp(1))
            End Select

        Next
        'doc.Properties("DocumentTitle") = docName
        doc.MimeType = mimeType

        If withContent = True Then
            Dim cel As IContentElementList = CreateContentElementList(file)
            If cel IsNot Nothing Then doc.ContentElements = cel
        End If

        Return doc
    End Function
    Public Shared Function createlist(value As String) As IStringList
        Dim slTo As IStringList = Factory.StringList.CreateList()
        Dim MultiValue = Split(value, ",")
        For I = 0 To UBound(MultiValue)
            slTo.Add(MultiValue(I))
        Next
        Return slTo
    End Function
    Public Shared Function createlistint(value As String) As IInteger32List
        Dim slTo As IInteger32List = Factory.Integer32List.CreateList()
        Dim MultiValue = Split(value, ",")
        For I = 0 To UBound(MultiValue)
            slTo.Add(Integer.Parse(MultiValue(I)))
        Next
        Return slTo
    End Function
    Public Shared Sub checkInDoc(ByVal doc As IDocument)
        'doc.Checkin(AutoClassify.AUTO_CLASSIFY, CheckinType.MINOR_VERSION)
        doc.Checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION)
        doc.Save(RefreshMode.REFRESH)
        doc.Refresh()
    End Sub

    Public Shared Function FetchDocByID(ByVal os As IObjectStore, ByVal id As String) As IDocument
        Dim ID2 As Id = New Id(id)
        Dim doc As IDocument
        Try
            doc = Factory.Document.FetchInstance(os, ID2, Nothing)
        Catch ex As Exception
            Return Nothing
        End Try

        Return doc
    End Function

    Public Shared Function FetchDocByPath(ByVal os As IObjectStore, ByVal path As String) As IDocument
        Dim doc As IDocument = Factory.Document.FetchInstance(os, path, Nothing)
        Return doc
    End Function

    Public Shared Function CreateCustomObject(ByVal os As IObjectStore, ByVal className As String) As ICustomObject
        Dim co As ICustomObject = Nothing

        If className.Equals("") Then
            co = Factory.CustomObject.CreateInstance(os, Nothing)
        Else
            co = Factory.CustomObject.CreateInstance(os, className)
        End If

        Return co
    End Function

    Public Shared Function FetchCustomObjectByPath(ByVal os As IObjectStore, ByVal path As String) As ICustomObject
        Dim co As ICustomObject = Factory.CustomObject.FetchInstance(os, path, Nothing)
        Return co
    End Function

    Public Shared Function FetchCustomObjectById(ByVal os As IObjectStore, ByVal id As String) As ICustomObject
        Dim ID2 As Id = New Id(id)
        Dim co As ICustomObject = Factory.CustomObject.FetchInstance(os, ID2, Nothing)
        Return co
    End Function

    Public Shared Function GetContainableProperties(ByVal c As IContainable) As Hashtable
        Dim properties As Hashtable = New Hashtable()
        properties.Add("ID", c.Id.ToString())
        properties.Add("Name", c.Name)
        properties.Add("Creator", c.Creator)
        properties.Add("Owner", c.Owner)
        properties.Add("Date Created", c.DateCreated.ToString())
        properties.Add("Date Last Modified", c.DateLastModified.ToString())
        Return properties
    End Function

    Public Shared Function GetContainablePermissions(ByVal c As IContainable) As ArrayList
        Dim permissions As ArrayList = New ArrayList()
        Dim acl As IAccessPermissionList = c.Permissions
        Dim ie As IEnumerator = acl.GetEnumerator()

        While ie.MoveNext()
            Dim ac As IAccessPermission = CType(ie.Current, IAccessPermission)
            permissions.Add("GRANTEE_NAME: " & ac.GranteeName)
            permissions.Add("ACCESS_MASK: " & ac.AccessMask.ToString())
            permissions.Add("ACCESS_TYPE: " & ac.AccessType.ToString())
        End While

        Return permissions
    End Function

    Public Shared Function FileContainable(ByVal os As IObjectStore, ByVal c As IContainable, ByVal folder As String) As IReferentialContainmentRelationship
        Dim f As IFolder = Factory.Folder.FetchInstance(os, folder, Nothing)
        Dim rcr As IReferentialContainmentRelationship = Nothing

        If TypeOf c Is IDocument Then
            rcr = f.File(CType(c, IDocument), AutoUniqueName.AUTO_UNIQUE, (CType(c, IDocument)).Name, DefineSecurityParentage.DO_NOT_DEFINE_SECURITY_PARENTAGE)
        Else
            rcr = f.File(CType(c, ICustomObject), AutoUniqueName.AUTO_UNIQUE, (CType(c, ICustomObject)).Name, DefineSecurityParentage.DO_NOT_DEFINE_SECURITY_PARENTAGE)
        End If

        Return rcr
    End Function

    Public Shared Function CreateFolder(ByVal os As IObjectStore, ByVal fPath As String, ByVal fName As String, ByVal className As String) As IFolder
        Dim f As IFolder = Factory.Folder.FetchInstance(os, fPath, Nothing)
        Dim nf As IFolder = Nothing

        If className.Equals("") Then
            nf = Factory.Folder.CreateInstance(os, Nothing)
        Else
            nf = Factory.Folder.CreateInstance(os, className)
        End If

        nf.FolderName = fName
        nf.Parent = f
        Return nf
    End Function

    Public Shared Function CreateComponentRelationship(ByVal os As IObjectStore, ByVal pTitle As String, ByVal cTitle As String) As IComponentRelationship
        Dim parent As IDocument = Factory.Document.CreateInstance(os, Nothing)
        parent.Properties("DocumentTitle") = pTitle
        parent.CompoundDocumentState = CompoundDocumentState.COMPOUND_DOCUMENT
        parent.Save(RefreshMode.REFRESH)
        parent.Checkin(AutoClassify.AUTO_CLASSIFY, CheckinType.MINOR_VERSION)
        parent.Save(RefreshMode.REFRESH)
        Dim child As IDocument = Factory.Document.CreateInstance(os, Nothing)
        child.Properties("DocumentTitle") = cTitle
        child.CompoundDocumentState = CompoundDocumentState.COMPOUND_DOCUMENT
        child.Save(RefreshMode.REFRESH)
        child.Checkin(AutoClassify.AUTO_CLASSIFY, CheckinType.MINOR_VERSION)
        child.Save(RefreshMode.REFRESH)
        Dim cr As IComponentRelationship = Factory.ComponentRelationship.CreateInstance(os, Nothing)
        cr.ParentComponent = parent
        cr.ChildComponent = child
        cr.ComponentRelationshipType = ComponentRelationshipType.DYNAMIC_CR
        cr.VersionBindType = VersionBindType.LATEST_VERSION
        Return cr
    End Function

    Public Shared Function FetchComponenetRelationship(ByVal os As IObjectStore, ByVal id As String) As IComponentRelationship
        Dim ID2 As Id = New Id(id)
        Dim cr As IComponentRelationship = Factory.ComponentRelationship.FetchInstance(os, ID2, Nothing)
        Return cr
    End Function

    Public Shared Function GetComponentRelationshipProperties(ByVal cr As IComponentRelationship) As Hashtable
        Dim properties As Hashtable = New Hashtable()
        properties.Add("ID", cr.Id.ToString())
        properties.Add("Creator", cr.Creator)
        properties.Add("Date Created", cr.DateCreated.ToString())
        properties.Add("Date Last Modified", cr.DateLastModified.ToString())
        properties.Add("Parent Component", cr.ParentComponent.Name)
        properties.Add("Child Component", cr.ChildComponent.Name)
        Return properties
    End Function

    Public Shared Function FetchResultRowSet(ByVal os As IObjectStore, ByVal [select] As String, ByVal from As String, ByVal where As String, ByVal rows As Integer) As IRepositoryRowSet
        Dim sq As SearchSQL = New SearchSQL()
        Dim ss As SearchScope = New SearchScope(os)
        sq.SetSelectList([select])
        sq.SetFromClauseInitialValue(from, Nothing, False)
        If Not (where.Equals("")) Then sq.SetWhereClause(where)
        If Not (rows = 0) Then sq.SetMaxRecords(rows)
        Dim rrs As IRepositoryRowSet = ss.FetchRows(sq, Nothing, Nothing, Nothing)
        Return rrs
    End Function
    Public Shared Function test(ByVal os As IObjectStore)
        Dim sq As SearchSQL = New SearchSQL()
        Dim ss As SearchScope = New SearchScope(os)
        sq.SetMaxRecords(1)
        sq.SetQueryString("SELECT R.ContainmentName, R.Tail, D.This FROM Document AS D WITH INCLUDESUBCLASSES INNER JOIN ReferentialContainmentRelationship AS R WITH INCLUDESUBCLASSES ON D.This = R.Head WHERE DocumentTitle like 'Test'")
        Dim rrs As IRepositoryRowSet = ss.FetchRows(sq, Nothing, Nothing, Nothing)
        Return rrs

    End Function
    Public Shared Function GetResultRow(ByVal rr As IRepositoryRow) As String()
        Dim prop As IProperties = rr.Properties
        Dim ie As IEnumerator = prop.GetEnumerator()
        Dim row As String() = New String(prop.Count - 1) {}
        Dim i As Integer = 0

        While ie.MoveNext()
            Dim p As IProperty = CType(ie.Current, IProperty)
            Dim value As Object = p.GetObjectValue()

            If value Is Nothing Then
                row(i) = "null"
            ElseIf TypeOf value Is IEngineCollection Then
                row(i) = "*"
            Else
                row(i) = value.ToString()
            End If

            i += 1
        End While

        Return row
    End Function

    Public Shared Function GetRowPropertiesName(ByVal rr As IRepositoryRow) As ArrayList
        Dim names As ArrayList = New ArrayList()
        Dim prop As IProperties = rr.Properties
        Dim ie As IEnumerator = prop.GetEnumerator()

        While ie.MoveNext()
            Dim p As IProperty = CType(ie.Current, IProperty)
            Dim name As String = p.GetPropertyName()
            names.Add(name)
        End While

        Return names
    End Function
    Public Shared Function FtpIbm(ByVal os As IObjectStore, lotto As String, PurchaseOrder As String, ItemNo As String, Finished As Int32, VMIOrder As String, OrdineCamp As String)



        Dim aa = os.Id.ToString
        Dim sq As SearchSQL = New SearchSQL()
        Dim ss As SearchScope = New SearchScope(os)
        sq.SetMaxRecords(1)
        'sq.SetQueryString("SELECT R.ContainmentName, R.Tail, D.This FROM Document AS D WITH INCLUDESUBCLASSES INNER JOIN ReferentialContainmentRelationship AS R WITH INCLUDESUBCLASSES ON D.This = R.Head WHERE DocumentTitle like 'Test'")
        Dim id
        ' OK sq.SetQueryString("SELECT R.ContainmentName, R.Tail, D.This FROM Document AS D WITH INCLUDESUBCLASSES INNER JOIN ReferentialContainmentRelationship AS R WITH INCLUDESUBCLASSES ON D.This = R.Head WHERE R.Tail='{D0E7D57F-0000-C015-8A5F-4DA5D7FF2E80}'")
        Select Case Finished
            Case 0
                sq.SetQueryString("SELECT  id FROM DocumentFromSupplierFolderTemplate WHERE '" & lotto & "' IN [LotNo] and '" & PurchaseOrder & "' IN [PurchaseOrder] and '" & ItemNo & ".10" & "' IN [ItemNo]")
            Case 1
                sq.SetQueryString("SELECT  id FROM DocumentFromSupplierFolderTemplate WHERE '" & lotto & "' IN [LotNo] and '" & PurchaseOrder & "' IN [PurchaseOrder] and '" & ItemNo & ".10" & "' IN [ItemNo] and [QCApproved]='-'")

        End Select

        Dim rrsr As IRepositoryRowSet = ss.FetchRows(sq, Nothing, Nothing, Nothing)

        If rrsr.IsEmpty Then
            Select Case Finished
                Case 0
                    sq.SetQueryString("SELECT  id FROM DocumentFromSupplierFolderTemplate WHERE '" & lotto & "' IN [LotNo] and '" & PurchaseOrder & "' IN [PurchaseOrder] and '" & ItemNo & "' IN [ItemNo]")
                Case 1
                    sq.SetQueryString("SELECT  id FROM DocumentFromSupplierFolderTemplate WHERE '" & lotto & "' IN [LotNo] and '" & PurchaseOrder & "' IN [PurchaseOrder] and '" & ItemNo & "' IN [ItemNo] and [QCApproved]='-'")
            End Select
            rrsr = ss.FetchRows(sq, Nothing, Nothing, Nothing)
        End If




        If rrsr.IsEmpty Then
            Select Case Finished
                Case 0
                    sq.SetQueryString("SELECT  id FROM DocumentFromSupplierFolderTemplate WHERE '" & PurchaseOrder & "' IN [PurchaseOrder] and '" & ItemNo & "' IN [ItemNo]")
                Case 1
                    sq.SetQueryString("SELECT  id FROM DocumentFromSupplierFolderTemplate WHERE '" & PurchaseOrder & "' IN [PurchaseOrder] and '" & ItemNo & "' IN [ItemNo] and [QCApproved]='-'")
            End Select
            rrsr = ss.FetchRows(sq, Nothing, Nothing, Nothing)
        End If

        If VMIOrder <> "" Then
            If rrsr.IsEmpty Then
                Select Case Finished
                    Case 0
                        sq.SetQueryString("SELECT  id FROM DocumentFromSupplierFolderTemplate WHERE '" & VMIOrder & "' IN [PurchaseOrder] and '" & ItemNo & "' IN [ItemNo]")
                    Case 1
                        sq.SetQueryString("SELECT  id FROM DocumentFromSupplierFolderTemplate WHERE '" & VMIOrder & "' IN [PurchaseOrder] and '" & ItemNo & "' IN [ItemNo] and [QCApproved]='-'")
                End Select
            End If

            rrsr = ss.FetchRows(sq, Nothing, Nothing, Nothing)
        End If






        If PurchaseOrder = "Storico" Then
            If rrsr.IsEmpty Then
                sq.SetQueryString("SELECT  id FROM [Document]  WHERE '" & lotto & "' IN [LotNo]")
            End If
            rrsr = ss.FetchRows(sq, Nothing, Nothing, Nothing)
            Return rrsr
        End If


        If rrsr.IsEmpty Then
            Return rrsr
        End If




        For Each n As IRepositoryRow In rrsr
            id = n.Properties.GetIdValue("id")
        Next

        Dim str = id.ToString.Replace("{", "").Replace("}", "")
        sq.SetQueryString("Select R.ContainmentName, R.Tail, D.This, D.DocumentTitle, D.id FROM Document AS D WITH INCLUDESUBCLASSES INNER JOIN ReferentialContainmentRelationship AS R WITH INCLUDESUBCLASSES ON D.This = R.Head WHERE R.Tail='{" & str & "}'")

        Dim rrs As IRepositoryRowSet = ss.FetchRows(sq, Nothing, Nothing, Nothing)
        Return rrs
    End Function
    Public Shared Function FetchFolderByID(ByVal os As IObjectStore, ByVal id As String) As IFolder
        Dim ID2 As Id = New Id(id)
        Dim folder As IFolder = Factory.Folder.FetchInstance(os, ID2, Nothing)
        Return folder
    End Function
End Class


